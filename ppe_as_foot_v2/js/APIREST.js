	var url = "controleur/fonction.php";
		 //--------------------------------//
		//--------LIRE LES COOCKIES-------//
	   //--------------------------------//	
	if (document.cookie.length > 0){ 
		var texte="Id_joueur=";
		var table=document.cookie.split(/;/);
		var IDGETCOOK="";
		for (i=0;i<table.length;i++){
			if(table[i].indexOf(texte)!= -1){
				IDGETCOOK = table[i].substring(Number(texte.length + table[i].indexOf( texte)), table[i].length);
			}
		}	
	}
		 //--------------------------------//
		//-----CACHER LE BOUTON RETOUR----//
	   //--------------------------------//
	$( ".retour" ).hide();
		 //--------------------------------//
		//-----AFFICHER LES PERSONNES-----//
	   //--------------------------------//
	$('#modifdiv').on("click",'.retour',function(){
		$( "#mainContent" ).show();	//affiche les personnes
		$( "#ModifierDIV" ).empty();	//enleve le formulaire
		$( "#ModifierSuivi" ).empty(); //enleve le formulaire
		$( "#ModifierFinance" ).empty(); //enleve le formulaire
		$( ".retour" ).hide(1000);	//enleve le boutton
	});
		 //--------------------------------//
		//---VIDER LA DIV DAFFICHAGE------//
	   //--------------------------------//	
	function Vider() {
		$( "#mainContent" ).hide();	 //cache les personnes
		$( ".retour" ).show(1000);	//enleve le boutton
	}	
		 //--------------------------------//
		//------RELOAD LA PAGE------------//
	   //--------------------------------//	
	function reload() {
		location.reload();
		MessageModifPersonne();	
	}		
		 //--------------------------------//
		//-----JSON AJOUTER PERSONNE------//
	   //--------------------------------//
	function getJson(){
		return JSON.stringify({
			"Nom_Joueur":  			document.getElementById('Nom_Joueur').value,
			"password":  			document.getElementById('password').value,
			"Prenom_Joueur":  		document.getElementById('Prenom_Joueur').value,
			"Age_Joueur":  			document.getElementById('Age_Joueur').value,
			"Email_Joueur1":  		document.getElementById('Email_Joueur1').value,
			"Email_Joueur2":  		document.getElementById('Email_Joueur2').value,
			"TelFix_Joueur":  		document.getElementById('TelFix_Joueur').value,
			"Portable_Joueur":  	document.getElementById('Portable_Joueur').value,
			"NomMere_Joueur":  		document.getElementById('NomMere_Joueur').value,
			"NomPere_Joueur":  		document.getElementById('NomPere_Joueur').value,
			"PrenomPere_Joueur": 	document.getElementById('PrenomPere_Joueur').value,
			"PrenomMere_Joueur": 	document.getElementById('PrenomMere_Joueur').value,
			"PortablePere_Joueur": 	document.getElementById('PortablePere_Joueur').value,
			"PortableMere_Joueur": 	document.getElementById('PortableMere_Joueur').value,
			"TelFixPere_Joueur": 	document.getElementById('TelFixPere_Joueur').value,
			"TelFixMere_Joueur": 	document.getElementById('TelFixMere_Joueur').value,
			"Adresse": 				document.getElementById('Adresse').value,
			"Ville": 				document.getElementById('Ville').value,
			"Code_Postal": 			document.getElementById('Code_Postal').value,
			"Id_Rang": 				document.getElementById('Id_Rang').value,
		});	
	}	
		 //--------------------------------//
		//------JSON COORDONNEE-----------//
	   //--------------------------------//						
	function getJsonModify(){
		return JSON.stringify({
			"Nom_JoueurM":  		document.getElementById('Nom_JoueurM').value,
			"passwordM":  			document.getElementById('passwordM').value,
			"Prenom_JoueurM":  		document.getElementById('Prenom_JoueurM').value,
			"Age_JoueurM":  		document.getElementById('Age_JoueurM').value,
			"Email_Joueur1M":  		document.getElementById('Email_Joueur1M').value,
			"Email_Joueur2M":  		document.getElementById('Email_Joueur2M').value,
			"TelFix_JoueurM":  		document.getElementById('TelFix_JoueurM').value,
			"Portable_JoueurM":  	document.getElementById('Portable_JoueurM').value,
			"NomMere_JoueurM":  	document.getElementById('NomMere_JoueurM').value,
			"NomPere_JoueurM":  	document.getElementById('NomPere_JoueurM').value,
			"PrenomPere_JoueurM": 	document.getElementById('PrenomPere_JoueurM').value,
			"PrenomMere_JoueurM": 	document.getElementById('PrenomMere_JoueurM').value,
			"PortablePere_JoueurM": document.getElementById('PortablePere_JoueurM').value,
			"PortableMere_JoueurM": document.getElementById('PortableMere_JoueurM').value,
			"TelFixPere_JoueurM": 	document.getElementById('TelFixPere_JoueurM').value,
			"TelFixMere_JoueurM": 	document.getElementById('TelFixMere_JoueurM').value,
			"AdresseM": 			document.getElementById('AdresseM').value,
			"VilleM": 				document.getElementById('VilleM').value,
			"Code_PostalM": 		document.getElementById('Code_PostalM').value,
			"Id_RangM": 			document.getElementById('Id_RangM').value,
		});		
	}	
		 //--------------------------------//
		//-----JSON RECHERCHER------------//
	   //--------------------------------//
	function getJsonR(){
		return JSON.stringify({
			"Nom_JoueurR":  document.getElementById('Nom_JoueurR').value,
		});		
	}	
		 //--------------------------------//
		//---------JSON SUIVI-------------//
	   //--------------------------------//
	function getJsonSuivi(){
		return JSON.stringify({
			"Suivi_NumLicence":  		document.getElementById('Suivi_NumLicence').value,
			"Suivi_Notes":  			document.getElementById('Suivi_Notes').value,
			"Suivi_DateNaissance":  	document.getElementById('Suivi_DateNaissance').value,
			"Suivi_Categorie":  		document.getElementById('Suivi_Categorie').value,
			"Suivi_Dirigeant":  		document.getElementById('Suivi_Dirigeant').value,
			"Suivi_InscritNewsletter":  document.getElementById('Suivi_InscritNewsletter').value,
			"Suivi_DispoLicence":  		document.getElementById('Suivi_DispoLicence').value,
			"Suivi_Formation":  		document.getElementById('Suivi_Formation').value,
			"Champconfig1":  			document.getElementById('Champconfig1').value,
			"Champconfig2":  			document.getElementById('Champconfig2').value,
			"Champconfig3": 			document.getElementById('Champconfig3').value,
			"Champconfig4": 			document.getElementById('Champconfig4').value,
			"Champconfig5": 			document.getElementById('Champconfig5').value,
			"Suivi_Photo": 				document.getElementById('Suivi_Photo').value,
		});	
	}
		 //--------------------------------//
		//---------JSON FINANCE-----------//
	   //--------------------------------//
	function getJsonFinance(){
		return JSON.stringify({
			"Finances_Notes":  			document.getElementById('Finances_Notes').value,
			"Finance_Attestation":  	document.getElementById('Finance_Attestation').value,
			"Finance_APayer":  			document.getElementById('Finance_APayer').value,
			"Finance_RecuCotis":  		document.getElementById('Finance_RecuCotis').value,
			"Finance_SommePayee":  		document.getElementById('Finance_SommePayee').value,
			"Finance_PayementComplet":  document.getElementById('Finance_PayementComplet').value,
			"Finance_NumCheque":  		document.getElementById('Finance_NumCheque').value,
			"Finance_DepotBanque":  	document.getElementById('Finance_DepotBanque').value,
			"Finance_Renouvellement":  	document.getElementById('Finance_Renouvellement').value,
		});	
	}
		 //--------------------------------//
		//-------------GET----------------//
	   //--------------------------------//
	$(document).ready(function(){
		$.getJSON(url+"/users", function(result){
			$.each(result, function(i, field){
				$("#montableau").append(
				"<tr><td>" + JSON.stringify(field.Id_Joueur) 			+"</td>"+
				"<td>" + JSON.stringify(field.Nom_Joueur) 				+"</td>"+
				"<td>" + JSON.stringify(field.Prenom_Joueur)			+"</td>"+
				"<td>" + JSON.stringify(field.Id_Rang)					+"</td>"+
				"<td><input type='submit' IdGetById='"+field.Id_Joueur+"' value='Voir'"+
				"class='btn btn-primary Modifier' onclick='Vider()'></td>"+
				"<td><input type='submit' IdGetById='"+field.Id_Joueur+"' value='données finnanciere'"+
				"class='btn btn-secondary finance' onclick='Vider()'></td>"+
				"<td><input type='submit' IdGetById='"+field.Id_Joueur+"' value='suivit sportif'"+
				"class='btn btn-info suivi ' onclick='Vider()'></td>"+
				"<td><input type='submit' id='"+field.Id_Joueur+"' value='Supprimer'"+
				"class='btn btn-warning supprimer' id='supprimer'></td></tr>");
			});
		});
	});		
		 //--------------------------------//
		//-------------GET Cooronnee------//
	   //--------------------------------//
	$(document).ready(function(){
		$.getJSON(url+"/users/"+IDGETCOOK, function(result){
			$.each(result, function(i, field){
				$("#coordonneeP").append(
					"<table class='table table-striped'>"+  //table
					
					"<tr><td><label for='Nom' class=' control-label'>ID :</label></td>"+ //ID input
					"<td>"+ JSON.stringify(field.Id_Joueur) +"</td></tr>"+					
					
					"<tr><td><label for='password' class='control-label'>Mot de passe :</label></td>"+ //password input
					"<td>"+ JSON.stringify(field.password) +"</td></tr>"+ 
					
					"<tr><td><label for='Nom' class='control-label'>Nom :</label></td>"+ //Nom input
					"<td>"+ JSON.stringify(field.Nom_Joueur) +"</td></tr>"+ 
					
					"<tr><td><label for='Prenom' control-label'>Prenom :</label></td>"+ //Prenom input
					"<td>"+ JSON.stringify(field.Prenom_Joueur) +"</td></tr>"+ 
					
					"<tr><td><label for='Age' control-label'>Age :</label></td>"+ //Age input
					"<td>"+ JSON.stringify(field.Age_Joueur) +"</td></tr>"+ 
					
					"<tr><td><label for='Email' control-label'>Email 1 :</label></td>"+ //Email input
					"<td>"+ JSON.stringify(field.Email_Joueur1) +"</td></tr>"+ 

					"<tr><td><label for='Email' control-label'>Email 2 :</label></td>"+ //Email input
					"<td>"+ JSON.stringify(field.Email_Joueur2) +"</td></tr>"+ 
					
					"<tr><td><label for='TelFix' control-label'>TelFix :</label></td>"+ //TelFix input
					"<td>"+ JSON.stringify(field.TelFix_Joueur) +"</td></tr>"+ 
					
					"<tr><td><label for='Portable' control-label'>Portable :</label></td>"+ //Portable input
					"<td>"+ JSON.stringify(field.Portable_Joueur) +"</td></tr>"+ 
					
					"<tr><td><label for='NomMere_Joueur' control-label'>Nom Mere :</label></td>"+ //NomMere_Joueur input
					"<td>"+ JSON.stringify(field.NomMere_Joueur) +"</td></tr>"+ 

					"<tr><td><label for='NomPere_Joueur' control-label'>Nom Pere :</label></td>"+ //NomPere_Joueur input
					"<td>"+ JSON.stringify(field.NomPere_Joueur) +"</td></tr>"+ 
					
					"<tr><td><label for='PrenomPere_Joueur' control-label'>Prenom Pere :</label></td>"+ //PrenomPere_Joueur input
					"<td>"+ JSON.stringify(field.PrenomPere_Joueur) +"</td></tr>"+ 

					"<tr><td><label for='PrenomMere_Joueur' control-label'>PrenomMere_Joueur :</label></td>"+ //PrenomMere_Joueur input
					"<td>"+ JSON.stringify(field.PrenomMere_Joueur) +"</td></tr>"+ 

					"<tr><td><label for='PortablePere_Joueur' control-label'>Portable Pere :</label></td>"+ //PortablePere_Joueur input
					"<td>"+ JSON.stringify(field.PortablePere_Joueur) +"</td></tr>"+ 

					"<tr><td><label for='PortableMere_Joueur' control-label'>Portable Mere :</label></td>"+ //PortableMere_Joueur input
					"<td>"+ JSON.stringify(field.PortableMere_Joueur) +"</td></tr>"+ 

					"<tr><td><label for='TelFixPere_Joueur' control-label'>Tel Fix Pere :</label></td>"+ //TelFixPere_Joueur input
					"<td>"+ JSON.stringify(field.TelFixPere_Joueur) +"</td></tr>"+ 

					"<tr><td><label for='TelFixMere_Joueur' control-label'>Tel Fix Mere :</label></td>"+ //TelFixMere_Joueur input
					"<td>"+ JSON.stringify(field.TelFixMere_Joueur) +"</td></tr>"+ 
					
					"<tr><td><label for='Adresse' control-label'>Adresse :</label></td>"+ //Adresse input
					"<td>"+ JSON.stringify(field.Adresse) +"</td></tr>"+ 					
					
					"<tr><td><label for='Ville' control-label'>Ville :</label></td>"+ //Ville input
					"<td>"+ JSON.stringify(field.Ville) +"</td></tr>"+ 
					
					"<tr><td><label for='Code_Postal' control-label'>Code_Postal :</label></td>"+ //Code_Postal input
					"<td>"+ JSON.stringify(field.Code_Postal) +"</td></tr>"+ 

					"<tr><td><label for='Id_Rang' control-label'>Rang :</label></td>"+ //Id_Rang input
					"<td>"+ JSON.stringify(field.Id_Rang) +"</td></tr>"+ 
					
					"</table>");
			});
		});
	});	
		 //--------------------------------//
		//-------------GET By NAME--------//
	   //--------------------------------//
	$( "#Rechercher" ).click(function() {	
		var Nom_Joueur = document.getElementById("Nom_JoueurR").value; 
		var urlgetbyID = url+"/users/rechercher/"+ Nom_Joueur;
		$( "#marecherche" ).empty();		
		$(document).ready(function(){
			$.getJSON(urlgetbyID, function(result){
				$.each(result, function(i, field){
					$("#marecherche").append(
					"<thead><tr><th>ID :</th><th>Nom de famille :</th></tr></thead>"+
					"<tr><td>" + JSON.stringify(field.Id_Joueur) 				+"</td>"+
					"<td>" + JSON.stringify(field.Nom_Joueur) 				+"</td>"+
					"<td><input type='submit' IdGetById='"+field.Id_Joueur+"' value='Voir'"+
					"class='btn btn-primary Modifier' onclick='Vider()'></td>"+
					"<td><input type='submit' IdGetById='"+field.Id_Joueur+"' value='données finnanciere'"+
					"class='btn btn-secondary finance' onclick='Vider()'></td>"+
					"<td><input type='submit' IdGetById='"+field.Id_Joueur+"' value='suivit sportif'"+
					"class='btn btn-info suivi ' onclick='Vider()'></td>"+
					"<td><input type='submit' id='"+field.Id_Joueur+"' value='Supprimer'"+
					"class='btn btn-warning supprimer' id='supprimer'></td></tr>");
				});
			});
		});	
	});	
		 //--------------------------------//
		//-------------POST---------------//
	   //--------------------------------//	
	$( "#ajouter" ).click(function() {
		$.ajax({
			type: "POST",
			url: url+"/add",
			contentType: "application/json; charset=utf-8",
			data: getJson(),	
			async: true,
		});	
	});	
		 //--------------------------------//
		//----------DELETE----------------//
	   //--------------------------------//
	$('#montableau').on("click",'.supprimer',function(){
		var ID = $(this).attr("id");	
		if(valeur == '2'){ Licences(); }else{
			swal({
				title: "es-tu sur ?",
				text: "Tu ne pourra pas faire marche arrière !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Oui, Suprime le!",
				cancelButtonText: "En fait non !",
				closeOnConfirm: false,
				closeOnCancel: false
			},
			function(isConfirm){
				if (isConfirm) {
					swal("Deleted!", "La personne a été supprimé", "success");									
					$.ajax({
						type: "DELETE",
						contentType: "application/json",
						url: url+"/delete/"+ID,
					});
				} else {
					swal("Cancelled", "Opération Annulé", "error");
				}
			});		
		}		
	});		
		 //--------------------------------//
		//--GET BEFORE PUT Coordonnee-----//
	   //--------------------------------//
	$('#MaPage').on("click",'.Modifier',function(){
		var GetByID=$(this).attr("IdGetById");		
		var urlgetbyID = url+"/users/"+ GetByID;
		$(document).ready(function(){
			$.getJSON(urlgetbyID, function(result){
				$.each(result, function(i, field){							
					$("#ModifierDIV").append("<div class='getByID' id='getByID'>"+
					
					"<table class='table table-striped'>"+  //table
					
					"<tr><td><label for='Nom' class=' control-label'>ID :</label></td>"+ //ID input
					"<td>"+ JSON.stringify(field.Id_Joueur) +"</td></tr>"+					
					
					"<tr><td><label for='password' class='control-label'>Mot de passe :</label></td>"+ //password input
					"<td><input type='text' class='form-control' id='passwordM' name='passwordM' value="+ JSON.stringify(field.password) +"/></td></tr>"+ 
					
					"<tr><td><label for='Nom' class='control-label'>Nom :</label></td>"+ //Nom input
					"<td><input type='text' class='form-control' id='Nom_JoueurM' name='Nom_JoueurM' value="+ JSON.stringify(field.Nom_Joueur) +"/></td></tr>"+ 
					
					"<tr><td><label for='Prenom' control-label'>Prenom :</label></td>"+ //Prenom input
					"<td><input type='text' class='form-control' id='Prenom_JoueurM' name='Prenom_JoueurM'  value="+ JSON.stringify(field.Prenom_Joueur) +"/></td></tr>"+ 
					
					"<tr><td><label for='Age' control-label'>Age :</label></td>"+ //Age input
					"<td><input type='text' class='form-control' id='Age_JoueurM' name='Age_JoueurM'  value="+ JSON.stringify(field.Age_Joueur) +"/></td></tr>"+ 
					
					"<tr><td><label for='Email' control-label'>Email 1 :</label></td>"+ //Email input
					"<td><input type='text' class='form-control' id='Email_Joueur1M' name='Email_Joueur1M'  value="+ JSON.stringify(field.Email_Joueur1) +"/></td></tr>"+ 

					"<tr><td><label for='Email' control-label'>Email 2 :</label></td>"+ //Email input
					"<td><input type='text' class='form-control' id='Email_Joueur2M' name='Email_Joueur2M'  value="+ JSON.stringify(field.Email_Joueur2) +"/></td></tr>"+ 
					
					"<tr><td><label for='TelFix' control-label'>TelFix :</label></td>"+ //TelFix input
					"<td><input type='text' class='form-control' id='TelFix_JoueurM' name='TelFix_JoueurM'  value="+ JSON.stringify(field.TelFix_Joueur) +"/></td></tr>"+ 
					
					"<tr><td><label for='Portable' control-label'>Portable :</label></td>"+ //Portable input
					"<td><input type='text' class='form-control' id='Portable_JoueurM' name='Portable_JoueurM'  value="+ JSON.stringify(field.Portable_Joueur) +"/></td></tr>"+ 
					
					"<tr><td><label for='NomMere_Joueur' control-label'>Nom Mere :</label></td>"+ //NomMere_Joueur input
					"<td><input type='text' class='form-control' id='NomMere_JoueurM' name='NomMere_JoueurM'  value="+ JSON.stringify(field.NomMere_Joueur) +"/></td></tr>"+ 

					"<tr><td><label for='NomPere_Joueur' control-label'>Nom Pere :</label></td>"+ //NomPere_Joueur input
					"<td><input type='text' class='form-control' id='NomPere_JoueurM' name='NomPere_JoueurM'  value="+ JSON.stringify(field.NomPere_Joueur) +"/></td></tr>"+ 
					
					"<tr><td><label for='PrenomPere_Joueur' control-label'>Prenom Pere :</label></td>"+ //PrenomPere_Joueur input
					"<td><input type='text' class='form-control' id='PrenomPere_JoueurM' name='PrenomPere_JoueurM'  value="+ JSON.stringify(field.PrenomPere_Joueur) +"/></td></tr>"+ 

					"<tr><td><label for='PrenomMere_Joueur' control-label'>PrenomMere_Joueur :</label></td>"+ //PrenomMere_Joueur input
					"<td><input type='text' class='form-control' id='PrenomMere_JoueurM' name='PrenomMere_JoueurM'  value="+ JSON.stringify(field.PrenomMere_Joueur) +"/></td></tr>"+ 

					"<tr><td><label for='PortablePere_Joueur' control-label'>Portable Pere :</label></td>"+ //PortablePere_Joueur input
					"<td><input type='text' class='form-control' id='PortablePere_JoueurM' name='PortablePere_JoueurM'  value="+ JSON.stringify(field.PortablePere_Joueur) +"/></td></tr>"+ 

					"<tr><td><label for='PortableMere_Joueur' control-label'>Portable Mere :</label></td>"+ //PortableMere_Joueur input
					"<td><input type='text' class='form-control' id='PortableMere_JoueurM' name='PortableMere_JoueurM'  value="+ JSON.stringify(field.PortableMere_Joueur) +"/></td></tr>"+ 

					"<tr><td><label for='TelFixPere_Joueur' control-label'>Tel Fix Pere :</label></td>"+ //TelFixPere_Joueur input
					"<td><input type='text' class='form-control' id='TelFixPere_JoueurM' name='TelFixPere_JoueurM'  value="+ JSON.stringify(field.TelFixPere_Joueur) +"/></td></tr>"+ 

					"<tr><td><label for='TelFixMere_Joueur' control-label'>Tel Fix Mere :</label></td>"+ //TelFixMere_Joueur input
					"<td><input type='text' class='form-control' id='TelFixMere_JoueurM' name='TelFixMere_JoueurM'  value="+ JSON.stringify(field.TelFixMere_Joueur) +"/></td></tr>"+ 
					
					"<tr><td><label for='Adresse' control-label'>Adresse :</label></td>"+ //Adresse input
					"<td><input type='text' class='form-control' id='AdresseM' name='AdresseM'  value="+ JSON.stringify(field.Adresse) +"/></td></tr>"+ 					
					
					"<tr><td><label for='Ville' control-label'>Ville :</label></td>"+ //Ville input
					"<td><input type='text' class='form-control' id='VilleM' name='VilleM'  value="+ JSON.stringify(field.Ville) +"/></td></tr>"+ 
					
					"<tr><td><label for='Code_Postal' control-label'>Code_Postal :</label></td>"+ //Code_Postal input
					"<td><input type='number' class='form-control' id='Code_PostalM' name='Code_PostalM'  value="+ JSON.stringify(field.Code_Postal) +"/></td></tr>"+ 

					"<tr><td><label for='Id_Rang' control-label'>Rang :</label></td>"+ //Id_Rang input
					"<td><input type='number' class='form-control' id='Id_RangM' name='Id_RangM'  value="+ JSON.stringify(field.Id_Rang) +"/></td></tr>"+ 
					
					"</table>"+ //.table
					
					"<input type='submit' IdModif='"+field.Id_Joueur+"' id='modifier' value='Valider la mofication' class='btn btn-primary modifier col-md-offset-8' onclick='reload()'>"+
					
					"</div>");
				});
			});
		});		
	});	
		 //--------------------------------//
		//---GET BEFORE PUT suivit--------//
	   //--------------------------------//
	$('#MaPage').on("click",'.suivi',function(){
		var GetByID=$(this).attr("IdGetById");		
		var urlgetbyID = url+"/suivi/"+ GetByID;

		$.ajax( {
			type:'Get',
			url: urlgetbyID,
	        contentType: "application/json; charset=utf-8",
	        dataType: "json",

			success:function(result) {
				$.each(result, function(i, field){
					$("#ModifierSuivi").append("<div class='getByID' id='getByID'>"+
						
						"<table class='table table-striped'>"+  //table
						
						"<tr><td><label for='Nom' class=' control-label'>ID :</label></td>"+ //ID input
						"<td>"+ JSON.stringify(field.Id_Joueur) +"</td></tr>"+					
						
						"<tr><td><label for='Suivi_NumLicence' class='control-label'>Suivi_NumLicence :</label></td>"+ //Suivi_NumLicence input
						"<td><input type='text' class='form-control' id='Suivi_NumLicence' name='Suivi_NumLicence' value="+ JSON.stringify(field.Suivi_NumLicence) +"/></td></tr>"+ 
						
						"<tr><td><label for='Suivi_Notes' class='control-label'>Suivi_Notes :</label></td>"+ //Suivi_Notes input
						"<td><input type='text' class='form-control' id='Suivi_Notes' name='Suivi_Notes' value="+ JSON.stringify(field.Suivi_Notes) +"/></td></tr>"+ 
						
						"<tr><td><label for='Suivi_DateNaissance' control-label'>Suivi_DateNaissance :</label></td>"+ //Suivi_DateNaissance input
						"<td><input type='Date' class='form-control' id='Suivi_DateNaissance' name='Suivi_DateNaissance'  value="+ JSON.stringify(field.Suivi_DateNaissance) +"/></td></tr>"+ 
						
						"<tr><td><label for='Suivi_Categorie' control-label'>Suivi_Categorie :</label></td>"+ //Suivi_Categorie input
						"<td><input type='text' class='form-control' id='Suivi_Categorie' name='Suivi_Categorie'  value="+ JSON.stringify(field.Suivi_Categorie) +"/></td></tr>"+ 
						
						"<tr><td><label for='Suivi_Dirigeant' control-label'>Suivi_Dirigeant :</label></td>"+ //Suivi_Dirigeant input
						"<td><input type='text' class='form-control' id='Suivi_Dirigeant' name='Suivi_Dirigeant'  value="+ JSON.stringify(field.Suivi_Dirigeant) +"/></td></tr>"+ 

						"<tr><td><label for='Suivi_InscritNewsletter' control-label'>Suivi_InscritNewsletter:</label></td>"+ //Suivi_InscritNewsletter input
						"<td><input type='text' class='form-control' id='Suivi_InscritNewsletter' name='Suivi_InscritNewsletter'  value="+ JSON.stringify(field.Suivi_InscritNewsletter) +"/></td></tr>"+ 
						
						"<tr><td><label for='Suivi_DispoLicence' control-label'>Suivi_DispoLicence :</label></td>"+ //Suivi_DispoLicence input
						"<td><input type='text' class='form-control' id='Suivi_DispoLicence' name='Suivi_DispoLicence'  value="+ JSON.stringify(field.Suivi_DispoLicence) +"/></td></tr>"+ 
						
						"<tr><td><label for='Suivi_Formation' control-label'>Suivi_Formation :</label></td>"+ //Suivi_Formation input
						"<td><input type='text' class='form-control' id='Suivi_Formation' name='Suivi_Formation'  value="+ JSON.stringify(field.Suivi_Formation) +"/></td></tr>"+ 
						
						"<tr><td><label for='Champconfig1' control-label'>Champconfig1 :</label></td>"+ //Champconfig1 input
						"<td><input type='text' class='form-control' id='Champconfig1' name='Champconfig1'  value="+ JSON.stringify(field.Champconfig1) +"/></td></tr>"+ 

						"<tr><td><label for='Champconfig2' control-label'>Champconfig2 :</label></td>"+ //Champconfig2 input
						"<td><input type='text' class='form-control' id='Champconfig2' name='Champconfig2'  value="+ JSON.stringify(field.Champconfig2) +"/></td></tr>"+ 
						
						"<tr><td><label for='Champconfig3' control-label'>Champconfig3 :</label></td>"+ //Champconfig3 input
						"<td><input type='text' class='form-control' id='Champconfig3' name='Champconfig3'  value="+ JSON.stringify(field.Champconfig3) +"/></td></tr>"+ 

						"<tr><td><label for='Champconfig4' control-label'>Champconfig4 :</label></td>"+ //Champconfig4 input
						"<td><input type='text' class='form-control' id='Champconfig4' name='Champconfig4'  value="+ JSON.stringify(field.Champconfig4) +"/></td></tr>"+ 

						"<tr><td><label for='Champconfig5' control-label'>Champconfig5 :</label></td>"+ //Champconfig5 input
						"<td><input type='text' class='form-control' id='Champconfig5' name='Champconfig5'  value="+ JSON.stringify(field.Champconfig5) +"/></td></tr>"+ 

						"<tr><td><label for='Suivi_Photo' control-label'>Suivi_Photo :</label></td>"+ //Suivi_Photo input
						"<td><input type='text' class='form-control' id='Suivi_Photo' name='Suivi_Photo'  value="+ JSON.stringify(field.Suivi_Photo) +"/></td></tr>"+ 
						
						"</table>"+ //.table
						
						"<input type='submit' IdModifSuivit='"+field.Id_Joueur+"' id='modifierSuivitbutton' value='Valider la mofication' class='btn btn-primary col-md-offset-8 modifierSuivitbutton' onclick='reload()'>"+
						
						"</div>");
				});	
			}
		})
	});	
		 //--------------------------------//
		//---GET BEFORE PUT finance--------//
	   //--------------------------------//
	$('#MaPage').on("click",'.finance',function(){
		var GetByID=$(this).attr("IdGetById");		
		var urlgetbyID = url+"/finance/"+ GetByID;
		$.ajax( {
			type:'Get',
			url: urlgetbyID,
	        contentType: "application/json; charset=utf-8",
	        dataType: "json",

			success:function(result) {

				$.each(result, function(i, field){

					$("#ModifierFinance").append("<div class='getByID' id='getByID'>"+
						
						"<table class='table table-striped'>"+  //table
						
						"<tr><td><label for='Nom' class=' control-label'>ID :</label></td>"+ //ID input
						"<td>"+ JSON.stringify(field.Id_Joueur) +"</td></tr>"+					
						
						"<tr><td><label for='Finances_Notes' class='control-label'>Finances_Notes :</label></td>"+ //Finances_Notes input
						"<td><input type='text' class='form-control' id='Finances_Notes' name='Finances_Notes' value="+ JSON.stringify(field.Finances_Notes) +"/></td></tr>"+ 
						
						"<tr><td><label for='Finance_Attestation' class='control-label'>Finance_Attestation :</label></td>"+ //Finance_Attestation input
						"<td><input type='text' class='form-control' id='Finance_Attestation' name='Finance_Attestation' value="+ JSON.stringify(field.Finance_Attestation) +"/></td></tr>"+ 
						
						"<tr><td><label for='Finance_APayer' control-label'>Finance_APayer :</label></td>"+ //Finance_APayer input
						"<td><input type='number' class='form-control' id='Finance_APayer' name='Finance_APayer'  value="+ JSON.stringify(field.Finance_APayer) +"/></td></tr>"+ 
						
						"<tr><td><label for='Finance_RecuCotis' control-label'>Finance_RecuCotis :</label></td>"+ //Finance_RecuCotis input
						"<td><input type='text' class='form-control' id='Finance_RecuCotis' name='Finance_RecuCotis'  value="+ JSON.stringify(field.Finance_RecuCotis) +"/></td></tr>"+ 
						
						"<tr><td><label for='Finance_SommePayee' control-label'>Finance_SommePayee :</label></td>"+ //Finance_SommePayee input
						"<td><input type='number' class='form-control' id='Finance_SommePayee' name='Finance_SommePayee'  value="+ JSON.stringify(field.Finance_SommePayee) +"/></td></tr>"+ 

						"<tr><td><label for='Finance_PayementComplet' control-label'>Finance_PayementComplet:</label></td>"+ //Finance_PayementComplet input
						"<td><input type='text' class='form-control' id='Finance_PayementComplet' name='Finance_PayementComplet'  value="+ JSON.stringify(field.Finance_PayementComplet) +"/></td></tr>"+ 
						
						"<tr><td><label for='Finance_NumCheque' control-label'>Finance_NumCheque :</label></td>"+ //Finance_NumCheque input
						"<td><input type='text' class='form-control' id='Finance_NumCheque' name='Finance_NumCheque'  value="+ JSON.stringify(field.Finance_NumCheque) +"/></td></tr>"+ 
						
						"<tr><td><label for='Finance_DepotBanque' control-label'>Finance_DepotBanque :</label></td>"+ //Finance_DepotBanque input
						"<td><input type='text' class='form-control' id='Finance_DepotBanque' name='Finance_DepotBanque'  value="+ JSON.stringify(field.Finance_DepotBanque) +"/></td></tr>"+ 
						
						"<tr><td><label for='Finance_Renouvellement' control-label'>Finance_Renouvellement :</label></td>"+ //Finance_Renouvellement input
						"<td><input type='text' class='form-control' id='Finance_Renouvellement' name='Finance_Renouvellement'  value="+ JSON.stringify(field.Finance_Renouvellement) +"/></td></tr>"+ 
						
						"</table>"+ //.table
						
						"<input type='submit' IdModifFinance='"+field.Id_Joueur+"' id='modifierfinanceid' value='Valider la mofication' class='btn btn-primary col-md-offset-8 modifierfinancebutton' onclick='reload()' >"+
						
						"</div>");
				});	
			}
		})
	});		
		 //--------------------------------//
		//----------PUT Coordonnee--------//
	   //--------------------------------//
	$('#ModifierDIV').on("click",'.modifier',function(){
		var IdModif=$(this).attr("IdModif");			
		$.ajax({
			data: getJsonModify(),
			type: "PUT",
			contentType: "application/json",
			url:  url+"/updates/Coordonnee/"+IdModif,
		});	
	});	
		 //--------------------------------//
		//------------PUT suivit----------//
	   //--------------------------------//
	$('#ModifierSuivi').on("click",'.modifierSuivitbutton',function(){
		var IdModif=$(this).attr("IdModifSuivit");		
		$.ajax({
			data: getJsonSuivi(),
			type: "PUT",
			contentType: "application/json",
			url:  url+"/updates/suivit/"+IdModif,
		});	
	});	
		 //--------------------------------//
		//-----------PUT Finance----------//
	   //--------------------------------//
	$('#ModifierFinance').on("click",'.modifierfinancebutton',function(){
		var IdModif=$(this).attr("IdModifFinance");		
		$.ajax({
			data: getJsonFinance(),
			type: "PUT",
			contentType: "application/json",
			url:  url+"/updates/finance/"+IdModif,
		});	
	});	