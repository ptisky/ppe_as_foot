	function Licences(){
		swal({
			title: "Vous n'avez pas la permission!",
			type: "info",
			confirmButtonClass: 'btn-primary'
		});
	}
	
	function ConnexionMessageFAIL(){
		swal({
			title: "mauvaise combinaison mot de passe/ID..",
			type: "info",
			confirmButtonClass: 'btn-primary',
		});	
	}

	function ConnexionMessageok(){
		swal({
			title: "Connexion Réussit !",
			type: "info",
			confirmButtonClass: 'btn-primary',
		});	
	}

	function MessageModifPersonne(){
		swal({
			title: "Personne Modifié !",
			type: "info",
			confirmButtonClass: 'btn-primary',
		});	
	}
