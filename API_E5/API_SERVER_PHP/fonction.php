<?php
require 'modele/connec_bdd.php';
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->error(function (\Exception $e) use ($app) {
    $app->render('error.php');
});

  //--------------------------------//
 //-----------ROUTES---------------//
//--------------------------------//
$app->get('/users','GetUsers');
$app->get('/users/:IdGetUsersByID','GetUsersByID');
$app->get('/suivi/:IdGetSuivitByID','GetSuivitByID');
$app->get('/finance/:IdGetFinanceByID','GetFinanceByID');
$app->get('/users/rechercher/:Nom_Joueur','GetUsersByName');

$app->post('/add', 'InsertUsers');

$app->delete('/delete/:Id','DeleteUser');

$app->put('/updates/Coordonnee/:IdModifyUser','ModifyUser');
$app->put('/updates/suivit/:IdModifySuivit','ModifySuivit');
$app->put('/updates/finance/:IdModifyFinance','ModifyFinance');

$app->run();

  //--------------------------------//
 //-------------GET----------------//
//--------------------------------//
function GetUsers() {
	$sql = "SELECT * FROM coordonnees ORDER BY Id_Joueur ASC";
	try {
		$bdd = getDB();
		$stmt = $bdd->query($sql); 
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$bdd = null;
		echo json_encode($users);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}';
	}
}
  //--------------------------------//
 //-------------POST---------------//
//--------------------------------//
function InsertUsers() {
		$today = date("m.d.y");
		$request = \Slim\Slim::getInstance()->request();
		$personne = json_decode($request ->getBody());
		$bdd = getDB();	
		$stmt = $bdd->prepare("INSERT INTO coordonnees(	Nom_Joueur,
														password,
														Prenom_Joueur,
														Age_Joueur,
														Email_Joueur1,
														Email_Joueur2,
														TelFix_Joueur,
														Portable_Joueur,
														NomMere_Joueur,
														NomPere_Joueur,
														PrenomPere_Joueur,
														PrenomMere_Joueur,
														PortablePere_Joueur,
														PortableMere_Joueur,
														TelFixPere_Joueur,
														TelFixMere_Joueur,
														Adresse,
														Ville,
														Code_Postal,
														Id_Rang)
														VALUES(	:Nom_Joueur,
																:password,
																:Prenom_Joueur,
																:Age_Joueur,
																:Email_Joueur1,
																:Email_Joueur2,
																:TelFix_Joueur,
																:Portable_Joueur,
																:NomMere_Joueur,
																:NomPere_Joueur,
																:PrenomPere_Joueur,
																:PrenomMere_Joueur,
																:PortablePere_Joueur,
																:PortableMere_Joueur,
																:TelFixPere_Joueur,
																:TelFixMere_Joueur,
																:Adresse,
																:Ville,
																:Code_Postal,
																:Id_Rang)");

		$stmt->bindParam(':Nom_Joueur', 			$personne->Nom_Joueur );
		$stmt->bindParam(':password', 				$personne->password );
		$stmt->bindParam(':Prenom_Joueur', 			$personne->Prenom_Joueur );
		$stmt->bindParam(':Age_Joueur', 			$personne->Age_Joueur );
		$stmt->bindParam(':Email_Joueur1', 			$personne->Email_Joueur1 );
		$stmt->bindParam(':Email_Joueur2', 			$personne->Email_Joueur2 );
		$stmt->bindParam(':TelFix_Joueur', 			$personne->TelFix_Joueur );
		$stmt->bindParam(':Portable_Joueur', 		$personne->Portable_Joueur );
		$stmt->bindParam(':NomMere_Joueur', 		$personne->NomMere_Joueur );
		$stmt->bindParam(':NomPere_Joueur', 		$personne->NomPere_Joueur );
		$stmt->bindParam(':PrenomPere_Joueur', 		$personne->PrenomPere_Joueur );
		$stmt->bindParam(':PrenomMere_Joueur', 		$personne->PrenomMere_Joueur );
		$stmt->bindParam(':PortablePere_Joueur', 	$personne->PortablePere_Joueur );
		$stmt->bindParam(':PortableMere_Joueur', 	$personne->PortableMere_Joueur );
		$stmt->bindParam(':TelFixPere_Joueur', 		$personne->TelFixPere_Joueur );
		$stmt->bindParam(':TelFixMere_Joueur', 		$personne->TelFixMere_Joueur );
		$stmt->bindParam(':Adresse', 				$personne->Adresse );
		$stmt->bindParam(':Ville', 					$personne->Ville );
		$stmt->bindParam(':Code_Postal', 			$personne->Code_Postal );
		$stmt->bindParam(':Id_Rang', 				$personne->Id_Rang );

        $result = $stmt->execute();
		echo json_encode($personne, JSON_PRETTY_PRINT);
 		$MonId = $bdd->lastInsertId();	

		$stmtSuivit = $bdd->prepare("INSERT INTO suivi(			Id_Suivi,
																Suivi_NumLicence,
																Suivi_Notes,
																Suivi_DateNaissance,
																Suivi_Categorie,
																Suivi_Dirigeant,
																Suivi_InscritNewsletter,
																Suivi_DispoLicence,
																Suivi_Formation,
																Champconfig1,
																Champconfig2,
																Champconfig3,
																Champconfig4,
																Champconfig5,
																Suivi_Photo,
																Id_Joueur)
																VALUES(	$MonId,
																		0,
																		0,
																		0,
																		0,
																		0,
																		0,
																		0,
																		0,
																		0,
																		0,
																		0,
																		0,
																		0,
																		0,
																		$MonId)");

		$stmtFinance = $bdd->prepare("INSERT INTO finances(		Id_Finances,
																Finances_Notes,
																Finance_Attestation,
																Finance_APayer,
																Finance_RecuCotis,
																Finance_SommePayee,
																Finance_PayementComplet,
																Finance_NumCheque,
																Finance_DepotBanque,
																Finance_Renouvellement,
																Id_Joueur)
																VALUES(	$MonId,
																		0,
																		0,
																		0,
																		0,
																		0,
																		0,
																		0,
																		0,
																		0,
																		$MonId)");

        $resultSuivit = $stmtSuivit->execute();
        $resultFinance = $stmtFinance->execute();
}
  //--------------------------------//
 //-----------DELETE---------------//
//--------------------------------//
function DeleteUser($Id) {
		$request = \Slim\Slim::getInstance()->request();
		$personne = json_decode($request ->getBody());
		$bdd = getDB();	
		$stmtsuivi = $bdd->prepare("Delete from suivi WHERE Id_Joueur = :Id");
		$stmtfinances = $bdd->prepare("Delete from finances WHERE Id_Joueur = :Id");
		$stmtcoordonnees = $bdd->prepare("Delete from coordonnees WHERE Id_Joueur = :Id");

		$stmtsuivi->bindParam(':Id', $Id );
		$stmtfinances->bindParam(':Id', $Id );
		$stmtcoordonnees->bindParam(':Id', $Id );

        $result = $stmtsuivi->execute();
		$result = $stmtfinances->execute();
		$result = $stmtcoordonnees->execute();
		$bdd = null;
		echo true;
}
  //--------------------------------//
 //--------PUT Coordonnee----------//
//--------------------------------//
function ModifyUser($IdModifyUser) {
		$bdd = getDB();	
		$request = \Slim\Slim::getInstance()->request();
		$personne = json_decode($request ->getBody());
		$stmt = $bdd->prepare("UPDATE coordonnees SET 	Nom_Joueur	 		= 	:Nom_Joueur, 
														password			= 	:password,
														Prenom_Joueur		= 	:Prenom_Joueur,
														Age_Joueur 			= 	:Age_Joueur, 
														Email_Joueur1 		= 	:Email_Joueur1, 
														Email_Joueur2 		= 	:Email_Joueur2, 
														TelFix_Joueur 		= 	:TelFix_Joueur, 
														Portable_Joueur 	= 	:Portable_Joueur, 
														NomMere_Joueur 		= 	:NomMere_Joueur, 
														NomPere_Joueur 		= 	:NomPere_Joueur, 
														PrenomPere_Joueur 	= 	:PrenomPere_Joueur, 
														PrenomMere_Joueur 	= 	:PrenomMere_Joueur, 
														PortablePere_Joueur = 	:PortablePere_Joueur, 
														PortableMere_Joueur = 	:PortableMere_Joueur, 
														TelFixPere_Joueur 	= 	:TelFixPere_Joueur, 
														TelFixMere_Joueur 	= 	:TelFixMere_Joueur, 												
														Adresse 			= 	:Adresse, 
														Adresse 			= 	:Adresse, 
														Ville 				= 	:Ville, 
														Code_Postal 		= 	:Code_Postal,
														Id_Rang 			= 	:Id_Rang 														
													WHERE Id_Joueur = :IdModifyUser");
		$stmt->bindParam(':IdModifyUser', $IdModifyUser );
		$stmt->bindParam(':Nom_Joueur', 			$personne->Nom_JoueurM );
		$stmt->bindParam(':password', 				$personne->passwordM );
		$stmt->bindParam(':Prenom_Joueur', 			$personne->Prenom_JoueurM );
		$stmt->bindParam(':Age_Joueur', 			$personne->Age_JoueurM );
		$stmt->bindParam(':Email_Joueur1', 			$personne->Email_Joueur1M );
		$stmt->bindParam(':Email_Joueur2', 			$personne->Email_Joueur2M );
		$stmt->bindParam(':TelFix_Joueur', 			$personne->TelFix_JoueurM );
		$stmt->bindParam(':Portable_Joueur', 		$personne->Portable_JoueurM );
		$stmt->bindParam(':NomMere_Joueur', 		$personne->NomMere_JoueurM );
		$stmt->bindParam(':NomPere_Joueur', 		$personne->NomPere_JoueurM );
		$stmt->bindParam(':PrenomPere_Joueur', 		$personne->PrenomPere_JoueurM );
		$stmt->bindParam(':PrenomMere_Joueur', 		$personne->PrenomMere_JoueurM );
		$stmt->bindParam(':PortablePere_Joueur', 	$personne->PortablePere_JoueurM );
		$stmt->bindParam(':PortableMere_Joueur', 	$personne->PortableMere_JoueurM );
		$stmt->bindParam(':TelFixPere_Joueur', 		$personne->TelFixPere_JoueurM );
		$stmt->bindParam(':TelFixMere_Joueur', 		$personne->TelFixMere_JoueurM );
		$stmt->bindParam(':Adresse', 				$personne->AdresseM );
		$stmt->bindParam(':Ville', 					$personne->VilleM );
		$stmt->bindParam(':Code_Postal', 			$personne->Code_PostalM );
		$stmt->bindParam(':Id_Rang', 				$personne->Id_RangM );
        $stmt->execute();
		echo json_encode($personne, JSON_PRETTY_PRINT);
		$bdd = null;
}
  //--------------------------------//
 //-------------PUT Suivit---------//
//--------------------------------//
function ModifySuivit($IdModifySuivit) {
		$bdd = getDB();	
		$request = \Slim\Slim::getInstance()->request();
		$personne = json_decode($request ->getBody());
		$stmt = $bdd->prepare("UPDATE suivi SET 		Suivi_NumLicence	 	= 	:Suivi_NumLicence, 
														Suivi_Notes				= 	:Suivi_Notes,
														Suivi_DateNaissance 	= 	:Suivi_DateNaissance, 
														Suivi_Categorie 		= 	:Suivi_Categorie, 
														Suivi_Dirigeant 		= 	:Suivi_Dirigeant, 
														Suivi_InscritNewsletter = 	:Suivi_InscritNewsletter, 
														Suivi_DispoLicence 		= 	:Suivi_DispoLicence, 
														Suivi_Formation 		= 	:Suivi_Formation, 
														Champconfig1 			= 	:Champconfig1, 
														Champconfig2 			= 	:Champconfig2, 
														Champconfig3 			= 	:Champconfig3, 
														Champconfig4 			= 	:Champconfig4, 
														Champconfig5 			= 	:Champconfig5, 
														Suivi_Photo 			= 	:Suivi_Photo
														WHERE Id_Suivi = :IdModifySuivit");
		$stmt->bindParam(':IdModifySuivit', 			$IdModifySuivit );
		$stmt->bindParam(':Suivi_NumLicence', 			$personne->Suivi_NumLicence );
		$stmt->bindParam(':Suivi_Notes', 				$personne->Suivi_Notes );
		$stmt->bindParam(':Suivi_DateNaissance', 		$personne->Suivi_DateNaissance );
		$stmt->bindParam(':Suivi_Categorie', 			$personne->Suivi_Categorie );
		$stmt->bindParam(':Suivi_Dirigeant', 			$personne->Suivi_Dirigeant );
		$stmt->bindParam(':Suivi_InscritNewsletter', 	$personne->Suivi_InscritNewsletter );
		$stmt->bindParam(':Suivi_DispoLicence', 		$personne->Suivi_DispoLicence );
		$stmt->bindParam(':Suivi_Formation', 			$personne->Suivi_Formation );
		$stmt->bindParam(':Champconfig1', 				$personne->Champconfig1 );
		$stmt->bindParam(':Champconfig2', 				$personne->Champconfig2 );
		$stmt->bindParam(':Champconfig3', 				$personne->Champconfig3 );
		$stmt->bindParam(':Champconfig4', 				$personne->Champconfig4 );
		$stmt->bindParam(':Champconfig5', 				$personne->Champconfig5 );
		$stmt->bindParam(':Suivi_Photo', 				$personne->Suivi_Photo );
        $stmt->execute();
		echo json_encode($personne, JSON_PRETTY_PRINT);
		$bdd = null;
}
  //--------------------------------//
 //------------PUT Finance---------//
//--------------------------------//
function ModifyFinance($IdModifyFinance) {
		$bdd = getDB();	
		$request = \Slim\Slim::getInstance()->request();
		$personne = json_decode($request ->getBody());
		$stmt = $bdd->prepare("UPDATE finances SET 		Finances_Notes	 		= 	:Finances_Notes, 
														Finance_Attestation		= 	:Finance_Attestation,
														Finance_APayer 			= 	:Finance_APayer, 
														Finance_RecuCotis 		= 	:Finance_RecuCotis, 
														Finance_SommePayee 		= 	:Finance_SommePayee, 
														Finance_PayementComplet = 	:Finance_PayementComplet, 
														Finance_NumCheque 		= 	:Finance_NumCheque, 
														Finance_DepotBanque 	= 	:Finance_DepotBanque, 
														Finance_Renouvellement 	= 	:Finance_Renouvellement 
														WHERE Id_Finances = :IdModifyFinance");
		$stmt->bindParam(':IdModifyFinance', 			$IdModifyFinance );
		$stmt->bindParam(':Finances_Notes', 			$personne->Finances_Notes );
		$stmt->bindParam(':Finance_Attestation', 		$personne->Finance_Attestation );
		$stmt->bindParam(':Finance_APayer', 			$personne->Finance_APayer );
		$stmt->bindParam(':Finance_RecuCotis', 			$personne->Finance_RecuCotis );
		$stmt->bindParam(':Finance_SommePayee', 		$personne->Finance_SommePayee );
		$stmt->bindParam(':Finance_PayementComplet', 	$personne->Finance_PayementComplet );
		$stmt->bindParam(':Finance_NumCheque', 			$personne->Finance_NumCheque );
		$stmt->bindParam(':Finance_DepotBanque', 		$personne->Finance_DepotBanque );
		$stmt->bindParam(':Finance_Renouvellement', 	$personne->Finance_Renouvellement );
        $stmt->execute();
		echo json_encode($personne, JSON_PRETTY_PRINT);
		$bdd = null;
}
  //--------------------------------//
 //------GET BY User ID------------//
//--------------------------------//
function GetUsersByID($IdGET) {
		$bdd = getDB();	
		$stmt = $bdd->prepare("SELECT * FROM coordonnees WHERE Id_Joueur = :IdGetUsersByID");
		$stmt->bindParam(':IdGetUsersByID', $IdGET );
        $result = $stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$bdd = null;
		echo json_encode($users);
}
  //--------------------------------//
 //--------GET Suivit BY ID--------//
//--------------------------------//
function GetSuivitByID($IdGET) {
		$bdd = getDB();	
		$stmt = $bdd->prepare("SELECT * FROM suivi WHERE Id_Suivi = :IdGetSuivitByID");
		$stmt->bindParam(':IdGetSuivitByID', $IdGET );
        $result = $stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$bdd = null;
		echo json_encode($users);
}
  //--------------------------------//
 //-------GET finance BY ID--------//
//--------------------------------//
function GetFinanceByID($IdGET) {
		$bdd = getDB();	
		$stmt = $bdd->prepare("SELECT * FROM finances WHERE Id_Finances = :IdGetFinanceByID");
		$stmt->bindParam(':IdGetFinanceByID', $IdGET );
        $result = $stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$bdd = null;
		echo json_encode($users);
}
  //--------------------------------//
 //----------GET BY Nom------------//
//--------------------------------//
function GetUsersByName($Nom_Joueur) {
		$bdd = getDB();	
		$stmt = $bdd->prepare("SELECT * FROM coordonnees WHERE Nom_Joueur = :Nom_Joueur");
		$stmt->bindParam(':Nom_Joueur', $Nom_Joueur );
        $result = $stmt->execute();
		$users = $stmt->fetchAll(PDO::FETCH_OBJ);
		$bdd = null;
		echo json_encode($users);
}
?>