package com.mkyong.rest;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.PreparedStatement;

public class SuiviDao extends AbstractDAO{
	
	  //--------------------------------//
	 //------GET BY Suivi ID-----------//
	//--------------------------------//
	public Suivi getSuiviById(int Id) throws SQLException, ClassNotFoundException{
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		System.out.println("-----------------tentative de requete SQL : select login, nom,prenom from adherent where ID=? ----------------");
		preparedStatement = (PreparedStatement) getConnection().prepareStatement("SELECT * FROM `suivi` WHERE Id_Suivi= ?;"); 
		preparedStatement.setInt(1, Id);
		resultSet=preparedStatement.executeQuery();
		String Suivi_NumLicence = null;
		String Suivi_Notes = null;
		String Suivi_DateNaissance = "01-01-1996";
		String Suivi_Categorie = null;
		String Suivi_Dirigeant = null;
		String Suivi_InscritNewsletter = null;
		String Suivi_DispoLicence = null;
		String Suivi_Formation = null;
		String Champconfig1 = null;
		String Champconfig2 = null;
		String Champconfig3 = null;
		String Champconfig4 = null;
		String Champconfig5 = null;
		String Suivi_Photo = null;
		int Id_Joueur = 0;
		while (resultSet.next()) {
			Id = resultSet.getInt("Id_Suivi");
			Suivi_NumLicence = resultSet.getString("Suivi_NumLicence");
			Suivi_Notes= resultSet.getString("Suivi_Notes");
			Suivi_DateNaissance= resultSet.getString("Suivi_DateNaissance");
			Suivi_Categorie= resultSet.getString("Suivi_Categorie");
			Suivi_Dirigeant= resultSet.getString("Suivi_Dirigeant");
			Suivi_InscritNewsletter= resultSet.getString("Suivi_InscritNewsletter");
			Suivi_DispoLicence= resultSet.getString("Suivi_DispoLicence");
			Suivi_Formation= resultSet.getString("Suivi_Formation");
			Champconfig1= resultSet.getString("Champconfig1");
			Champconfig2= resultSet.getString("Champconfig2");
			Champconfig3= resultSet.getString("Champconfig3");
			Champconfig4= resultSet.getString("Champconfig4");
			Champconfig5= resultSet.getString("Champconfig5");
			Suivi_Photo= resultSet.getString("Suivi_Photo");
			Id_Joueur= resultSet.getInt("Id_Joueur");
			System.out.println(Id+Suivi_NumLicence+Suivi_Notes+Suivi_DateNaissance+Suivi_Categorie+
					Suivi_Dirigeant+Suivi_InscritNewsletter+Suivi_DispoLicence+Suivi_Formation+Champconfig1+Champconfig2+
					Champconfig3+Champconfig4+Champconfig5+Suivi_Photo+Id_Joueur);
		}	
	   return(new Suivi(Id,Suivi_NumLicence,Suivi_Notes,Suivi_DateNaissance,Suivi_Categorie,Suivi_Dirigeant,Suivi_InscritNewsletter,
			   Suivi_DispoLicence,Suivi_Formation,Champconfig1,Champconfig2,Champconfig3,Champconfig4,Champconfig5,
			   Suivi_Photo,Id_Joueur));	
	}
	 //--------------------------------//
	 //-----------DELETE---------------//
	//--------------------------------//
	public void deleteSuivi(int Id)
	{
		try {
			this.getConnection();
	    System.out.println("-----------------tentative de requete SQL : DELETE FROM users WHERE login =? ----------------");		
		PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("DELETE FROM suivi WHERE Id_Joueur =?");
		preparedStatement.setInt(1, Id);
		preparedStatement.executeUpdate();
		System.out.println("ok.");
		preparedStatement.close();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("probleme de bdd");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection();
		}
	}
	//--------------------------------//
	//-------------POST---------------//
	//--------------------------------//
	public void insertSuivi(Suivi s)
	{
		try {
			this.getConnection();
	    System.out.println("-----------------tentative de requete SQL : INSERT INTO users VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ----------------");
		PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("INSERT INTO adherent VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		preparedStatement.setInt(1, s.getId_Suivi());
	    preparedStatement.setString(2, s.getSuivi_NumLicence());
	    preparedStatement.setString(3, s.getSuivi_Notes());
	    preparedStatement.setString(4, s.getSuivi_DateNaissance());
	    preparedStatement.setString(5, s.getSuivi_Categorie());
	    preparedStatement.setString(6, s.getSuivi_Dirigeant());
	    preparedStatement.setString(7, s.getSuivi_InscritNewsletter());
	    preparedStatement.setString(8, s.getSuivi_DispoLicence());
	    preparedStatement.setString(9, s.getSuivi_Formation());
	    preparedStatement.setString(10, s.getChampconfig1());
	    preparedStatement.setString(11, s.getChampconfig2());
	    preparedStatement.setString(12, s.getChampconfig3());
	    preparedStatement.setString(13, s.getChampconfig4());
	    preparedStatement.setString(14, s.getChampconfig5());
	    preparedStatement.setString(15, s.getSuivi_Photo());
	    preparedStatement.setInt(16, s.getId_Joueur());
	    preparedStatement.executeUpdate();
    	System.out.println("ok.");
	    preparedStatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("probleme de bdd");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection();
		}
	}
}
