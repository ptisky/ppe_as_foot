package com.mkyong.rest;

public class Finance {
	private int Id_Finances;
	private String  Finances_Notes;
	private String Finance_Attestation;
	private int Finance_APayer;
	private String Finance_RecuCotis;
	private int Finance_SommePayee;
	private String Finance_PayementComplet;
	private String Finance_NumCheque;
	private String Finance_DepotBanque;
	private String Finance_Renouvellement;
	private int Id_Joueur;

	public Finance(int id_Finances, String finances_Notes, String finance_Attestation, int finance_APayer,
			String finance_RecuCotis, int finance_SommePayee, String finance_PayementComplet, String finance_NumCheque,
			String finance_DepotBanque, String finance_Renouvellement, int id_Joueur) {
		Id_Finances = id_Finances;
		Finances_Notes = finances_Notes;
		Finance_Attestation = finance_Attestation;
		Finance_APayer = finance_APayer;
		Finance_RecuCotis = finance_RecuCotis;
		Finance_SommePayee = finance_SommePayee;
		Finance_PayementComplet = finance_PayementComplet;
		Finance_NumCheque = finance_NumCheque;
		Finance_DepotBanque = finance_DepotBanque;
		Finance_Renouvellement = finance_Renouvellement;
		Id_Joueur = id_Joueur;
	}
	public Finance(){	
	}	
	public int getId_Finances() {
		return Id_Finances;
	}
	public void setId_Finances(int id_Finances) {
		Id_Finances = id_Finances;
	}
	public String getFinances_Notes() {
		return Finances_Notes;
	}
	public void setFinances_Notes(String finances_Notes) {
		Finances_Notes = finances_Notes;
	}
	public String getFinance_Attestation() {
		return Finance_Attestation;
	}
	public void setFinance_Attestation(String finance_Attestation) {
		Finance_Attestation = finance_Attestation;
	}
	public int getFinance_APayer() {
		return Finance_APayer;
	}
	public void setFinance_APayer(int finance_APayer) {
		Finance_APayer = finance_APayer;
	}
	public String getFinance_RecuCotis() {
		return Finance_RecuCotis;
	}
	public void setFinance_RecuCotis(String finance_RecuCotis) {
		Finance_RecuCotis = finance_RecuCotis;
	}
	public int getFinance_SommePayee() {
		return Finance_SommePayee;
	}
	public void setFinance_SommePayee(int finance_SommePayee) {
		Finance_SommePayee = finance_SommePayee;
	}
	public String getFinance_PayementComplet() {
		return Finance_PayementComplet;
	}
	public void setFinance_PayementComplet(String finance_PayementComplet) {
		Finance_PayementComplet = finance_PayementComplet;
	}
	public String getFinance_NumCheque() {
		return Finance_NumCheque;
	}
	public void setFinance_NumCheque(String finance_NumCheque) {
		Finance_NumCheque = finance_NumCheque;
	}
	public String getFinance_DepotBanque() {
		return Finance_DepotBanque;
	}
	public void setFinance_DepotBanque(String finance_DepotBanque) {
		Finance_DepotBanque = finance_DepotBanque;
	}
	public String getFinance_Renouvellement() {
		return Finance_Renouvellement;
	}
	public void setFinance_Renouvellement(String finance_Renouvellement) {
		Finance_Renouvellement = finance_Renouvellement;
	}
	public int getId_Joueur() {
		return Id_Joueur;
	}
	public void setId_Joueur(int id_Joueur) {
		Id_Joueur = id_Joueur;
	}
}
