package com.mkyong.rest;

import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;

public class SuiviDao extends AbstractDAO{
	
	  //--------------------------------//
	 //------GET BY Suivi ID-----------//
	//--------------------------------//
	public ArrayList<Suivi> getSuiviById(int Id) throws SQLException, ClassNotFoundException
	{
		try{
			this.getConnection();
			System.out.println("-----------------tentative de requete SQL : SELECT * FROM adherent ----------------");
			PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT * FROM `suivi` WHERE Id_Suivi= ?;");
			preparedStatement.setInt(1, Id);
			resultSet = preparedStatement.executeQuery();
			ArrayList<Suivi> suivi = new ArrayList<Suivi>();
			while (resultSet.next()) {
				System.out.println("ok.");
				Suivi s = new Suivi();
				s.setId_Suivi(resultSet.getInt("Id_Suivi"));
				s.setSuivi_NumLicence (resultSet.getString("Suivi_NumLicence"));
				s.setSuivi_Notes( resultSet.getString("Suivi_Notes"));
				s.setSuivi_DateNaissance( resultSet.getString("Suivi_DateNaissance"));
				s.setSuivi_Categorie( resultSet.getString("Suivi_Categorie"));
				s.setSuivi_Dirigeant( resultSet.getString("Suivi_Dirigeant"));
				s.setSuivi_InscritNewsletter( resultSet.getString("Suivi_InscritNewsletter"));
				s.setSuivi_DispoLicence( resultSet.getString("Suivi_DispoLicence"));
				s.setSuivi_Formation( resultSet.getString("Suivi_Formation"));
				s.setChampconfig1( resultSet.getString("Champconfig1"));
				s.setChampconfig2( resultSet.getString("Champconfig2"));
				s.setChampconfig3( resultSet.getString("Champconfig3"));
				s.setChampconfig4( resultSet.getString("Champconfig4"));
				s.setChampconfig5( resultSet.getString("Champconfig5"));
				s.setSuivi_Photo( resultSet.getString("Suivi_Photo"));
				s.setId_Joueur( resultSet.getInt("Id_Joueur"));
				suivi.add(s);
			}
			this.closeConnection();
			return suivi;	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("probleme de bdd");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection();
		}
		return null;
	}
	
	 //--------------------------------//
	 //-----------DELETE---------------//
	//--------------------------------//
	public void deleteSuivi(int Id)
	{
		try {
			this.getConnection();
	    System.out.println("-----------------tentative de requete SQL : DELETE FROM users WHERE login =? ----------------");		
		PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("DELETE FROM suivi WHERE Id_Joueur =?");
		preparedStatement.setInt(1, Id);
		preparedStatement.executeUpdate();
		System.out.println("ok.");
		preparedStatement.close();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("probleme de bdd");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection();
		}
	}
	//--------------------------------//
	//-------------POST---------------//
	//--------------------------------//
	public void insertSuivi(Suivi s)
	{
		try {
			this.getConnection();
	    System.out.println("-----------------tentative de requete SQL : INSERT INTO users VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ----------------");
		PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("INSERT INTO adherent VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		preparedStatement.setInt(1, s.getId_Suivi());
	    preparedStatement.setString(2, s.getSuivi_NumLicence());
	    preparedStatement.setString(3, s.getSuivi_Notes());
	    preparedStatement.setString(4, s.getSuivi_DateNaissance());
	    preparedStatement.setString(5, s.getSuivi_Categorie());
	    preparedStatement.setString(6, s.getSuivi_Dirigeant());
	    preparedStatement.setString(7, s.getSuivi_InscritNewsletter());
	    preparedStatement.setString(8, s.getSuivi_DispoLicence());
	    preparedStatement.setString(9, s.getSuivi_Formation());
	    preparedStatement.setString(10, s.getChampconfig1());
	    preparedStatement.setString(11, s.getChampconfig2());
	    preparedStatement.setString(12, s.getChampconfig3());
	    preparedStatement.setString(13, s.getChampconfig4());
	    preparedStatement.setString(14, s.getChampconfig5());
	    preparedStatement.setString(15, s.getSuivi_Photo());
	    preparedStatement.setInt(16, s.getId_Joueur());
	    preparedStatement.executeUpdate();
    	System.out.println("ok.");
	    preparedStatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("probleme de bdd");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection();
		}
	}
}
