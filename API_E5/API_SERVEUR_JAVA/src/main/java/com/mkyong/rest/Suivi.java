package com.mkyong.rest;

public class Suivi {
	private int Id_Suivi;
	private String Suivi_NumLicence;
	private String Suivi_Notes;
	private String Suivi_DateNaissance;
	private String Suivi_Categorie;
	private String Suivi_Dirigeant;
	private String Suivi_InscritNewsletter;
	private String Suivi_DispoLicence;
	private String Suivi_Formation;
	private String Champconfig1;
	private String Champconfig2;
	private String Champconfig3;
	private String Champconfig4;
	private String Champconfig5;
	private String Suivi_Photo;
	private int Id_Joueur;
	
	public Suivi(int id_Suivi, String suivi_NumLicence, String suivi_Notes, String suivi_DateNaissance,
			String suivi_Categorie, String suivi_Dirigeant, String suivi_InscritNewsletter, String suivi_DispoLicence,
			String suivi_Formation, String champconfig1, String champconfig2, String champconfig3, String champconfig4,
			String champconfig5, String suivi_Photo, int id_Joueur) {
		Id_Suivi = id_Suivi;
		Suivi_NumLicence = suivi_NumLicence;
		Suivi_Notes = suivi_Notes;
		Suivi_DateNaissance = suivi_DateNaissance;
		Suivi_Categorie = suivi_Categorie;
		Suivi_Dirigeant = suivi_Dirigeant;
		Suivi_InscritNewsletter = suivi_InscritNewsletter;
		Suivi_DispoLicence = suivi_DispoLicence;
		Suivi_Formation = suivi_Formation;
		Champconfig1 = champconfig1;
		Champconfig2 = champconfig2;
		Champconfig3 = champconfig3;
		Champconfig4 = champconfig4;
		Champconfig5 = champconfig5;
		Suivi_Photo = suivi_Photo;
		Id_Joueur = id_Joueur;
	}
	public Suivi(){
		
	}		
	public int getId_Suivi() {
		return Id_Suivi;
	}
	public void setId_Suivi(int id_Suivi) {
		Id_Suivi = id_Suivi;
	}
	public String getSuivi_NumLicence() {
		return Suivi_NumLicence;
	}
	public void setSuivi_NumLicence(String suivi_NumLicence) {
		Suivi_NumLicence = suivi_NumLicence;
	}
	public String getSuivi_Notes() {
		return Suivi_Notes;
	}
	public void setSuivi_Notes(String suivi_Notes) {
		Suivi_Notes = suivi_Notes;
	}
	public String getSuivi_DateNaissance() {
		return Suivi_DateNaissance;
	}
	public void setSuivi_DateNaissance(String suivi_DateNaissance) {
		Suivi_DateNaissance = suivi_DateNaissance;
	}
	public String getSuivi_Categorie() {
		return Suivi_Categorie;
	}
	public void setSuivi_Categorie(String suivi_Categorie) {
		Suivi_Categorie = suivi_Categorie;
	}
	public String getSuivi_Dirigeant() {
		return Suivi_Dirigeant;
	}
	public void setSuivi_Dirigeant(String suivi_Dirigeant) {
		Suivi_Dirigeant = suivi_Dirigeant;
	}
	public String getSuivi_InscritNewsletter() {
		return Suivi_InscritNewsletter;
	}
	public void setSuivi_InscritNewsletter(String suivi_InscritNewsletter) {
		Suivi_InscritNewsletter = suivi_InscritNewsletter;
	}
	public String getSuivi_DispoLicence() {
		return Suivi_DispoLicence;
	}
	public void setSuivi_DispoLicence(String suivi_DispoLicence) {
		Suivi_DispoLicence = suivi_DispoLicence;
	}
	public String getSuivi_Formation() {
		return Suivi_Formation;
	}
	public void setSuivi_Formation(String suivi_Formation) {
		Suivi_Formation = suivi_Formation;
	}
	public String getChampconfig1() {
		return Champconfig1;
	}
	public void setChampconfig1(String champconfig1) {
		Champconfig1 = champconfig1;
	}
	public String getChampconfig2() {
		return Champconfig2;
	}
	public void setChampconfig2(String champconfig2) {
		Champconfig2 = champconfig2;
	}
	public String getChampconfig3() {
		return Champconfig3;
	}
	public void setChampconfig3(String champconfig3) {
		Champconfig3 = champconfig3;
	}
	public String getChampconfig4() {
		return Champconfig4;
	}
	public void setChampconfig4(String champconfig4) {
		Champconfig4 = champconfig4;
	}
	public String getChampconfig5() {
		return Champconfig5;
	}
	public void setChampconfig5(String champconfig5) {
		Champconfig5 = champconfig5;
	}
	public String getSuivi_Photo() {
		return Suivi_Photo;
	}
	public void setSuivi_Photo(String suivi_Photo) {
		Suivi_Photo = suivi_Photo;
	}
	public int getId_Joueur() {
		return Id_Joueur;
	}
	public void setId_Joueur(int id_Joueur) {
		Id_Joueur = id_Joueur;
	}
}
