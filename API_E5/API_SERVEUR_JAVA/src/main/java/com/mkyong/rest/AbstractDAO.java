package com.mkyong.rest;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;


abstract public class AbstractDAO {
	protected Connection connection = null;
	protected Statement statement = null;
	protected ResultSet resultSet = null;

	public Connection getConnection() throws ClassNotFoundException, SQLException 
	{
		try{
			System.out.println("-----------------run du driver----------------");
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("-----------------tentative de connexion----------------");
		    connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/ppeasfoot?" + "user=root&password=");
		    return connection;
		}catch(SQLException e){
			System.err.println("connection refus�");
		}
		return connection;
	}
	
	public void closeConnection()
	{
		try {
			System.out.println("-----------------tentative fermeture connexion----------------");
			connection.close();
		} catch (Exception e) {}
	}
}
