package com.mkyong.rest;

import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;
import com.mkyong.rest.AbstractDAO;
import com.mkyong.rest.User;

/**
 * 
 * @author Morgan
 *
 */

public class UserDAO extends AbstractDAO {



	  //--------------------------------//
	 //------GET BY User ID------------//
	//--------------------------------//
	public ArrayList<User> getUserById(int Id) throws SQLException, ClassNotFoundException
		{
			try{
				this.getConnection();
				System.out.println("-----------------tentative de requete SQL : SELECT * FROM adherent ----------------");
				PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT * FROM `coordonnees` WHERE Id_Joueur= ?;");
				preparedStatement.setInt(1, Id);
				resultSet = preparedStatement.executeQuery();
				ArrayList<User> utilisateurs = new ArrayList<User>();
				while (resultSet.next()) {
					System.out.println("ok.");
					User u = new User();
					u.setId_Joueur(resultSet.getInt("Id_Joueur"));
					u.setPassword (resultSet.getString("password"));
					u.setNom_Joueur( resultSet.getString("Nom_Joueur"));
					u.setPrenom_Joueur( resultSet.getString("Prenom_Joueur"));
					u.setAge_Joueur( resultSet.getString("Age_Joueur"));
					u.setEmail_Joueur1( resultSet.getString("Email_Joueur1"));
					u.setEmail_Joueur2( resultSet.getString("Email_Joueur2"));
					u.setTelFix_Joueur( resultSet.getString("TelFix_Joueur"));
					u.setPortable_Joueur( resultSet.getString("Portable_Joueur"));
					u.setNomPere_Joueur( resultSet.getString("NomPere_Joueur"));
					u.setNomMere_Joueur( resultSet.getString("NomMere_Joueur"));
					u.setPrenomPere_Joueur( resultSet.getString("PrenomPere_Joueur"));
					u.setPrenomMere_Joueur( resultSet.getString("PrenomMere_Joueur"));
					u.setPortablePere_Joueur( resultSet.getString("PortablePere_Joueur"));
					u.setPortableMere_Joueur( resultSet.getString("PortableMere_Joueur"));
					u.setTelFixPere_Joueur( resultSet.getString("TelFixPere_Joueur"));
					u.setTelFixMere_Joueur( resultSet.getString("TelFixMere_Joueur"));
					u.setAdresse( resultSet.getString("Adresse"));
					u.setVille( resultSet.getString("Ville"));
					u.setCode_Postal( resultSet.getInt("Code_Postal"));
					u.setId_Rang( resultSet.getInt("Id_Rang"));
					utilisateurs.add(u);
				}
				this.closeConnection();
				return utilisateurs;	
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("probleme de bdd");
			}catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				closeConnection();
			}
			return null;
		}
	
	  //--------------------------------//
	 //------GET BY User Name----------//
	//--------------------------------//
	public  ArrayList<User> getUserByName(String Nom) throws SQLException, ClassNotFoundException
		{
			try{
				this.getConnection();
				System.out.println("-----------------tentative de requete SQL : SELECT * FROM adherent ----------------");
				PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT * FROM `coordonnees` WHERE Nom_Joueur = ?;");
				preparedStatement.setString(1, Nom);
				resultSet = preparedStatement.executeQuery();
				ArrayList<User> utilisateurs = new ArrayList<User>();
				while (resultSet.next()) {
					System.out.println("ok.");
					User u = new User();
					u.setId_Joueur(resultSet.getInt("Id_Joueur"));
					u.setPassword (resultSet.getString("password"));
					u.setNom_Joueur( resultSet.getString("Nom_Joueur"));
					u.setPrenom_Joueur( resultSet.getString("Prenom_Joueur"));
					u.setAge_Joueur( resultSet.getString("Age_Joueur"));
					u.setEmail_Joueur1( resultSet.getString("Email_Joueur1"));
					u.setEmail_Joueur2( resultSet.getString("Email_Joueur2"));
					u.setTelFix_Joueur( resultSet.getString("TelFix_Joueur"));
					u.setPortable_Joueur( resultSet.getString("Portable_Joueur"));
					u.setNomPere_Joueur( resultSet.getString("NomPere_Joueur"));
					u.setNomMere_Joueur( resultSet.getString("NomMere_Joueur"));
					u.setPrenomPere_Joueur( resultSet.getString("PrenomPere_Joueur"));
					u.setPrenomMere_Joueur( resultSet.getString("PrenomMere_Joueur"));
					u.setPortablePere_Joueur( resultSet.getString("PortablePere_Joueur"));
					u.setPortableMere_Joueur( resultSet.getString("PortableMere_Joueur"));
					u.setTelFixPere_Joueur( resultSet.getString("TelFixPere_Joueur"));
					u.setTelFixMere_Joueur( resultSet.getString("TelFixMere_Joueur"));
					u.setAdresse( resultSet.getString("Adresse"));
					u.setVille( resultSet.getString("Ville"));
					u.setCode_Postal( resultSet.getInt("Code_Postal"));
					u.setId_Rang( resultSet.getInt("Id_Rang"));
					utilisateurs.add(u);
				}
				this.closeConnection();
				return utilisateurs;	
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("probleme de bdd");
			}catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				closeConnection();
			}
			return null;
		}
	
	  //--------------------------------//
	 //-------------GET----------------//
	//--------------------------------//
	public ArrayList<User> getAllUser() throws SQLException, ClassNotFoundException
	{
		try{
			this.getConnection();
			System.out.println("-----------------tentative de requete SQL : SELECT * FROM adherent ----------------");
			PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT * FROM coordonnees");
			resultSet = preparedStatement.executeQuery();
			ArrayList<User> utilisateurs = new ArrayList<User>();
			while (resultSet.next()) {
				System.out.println("ok.");
				User u = new User();
				u.setId_Joueur(resultSet.getInt("Id_Joueur"));
				u.setPassword (resultSet.getString("password"));
				u.setNom_Joueur( resultSet.getString("Nom_Joueur"));
				u.setPrenom_Joueur( resultSet.getString("Prenom_Joueur"));
				u.setAge_Joueur( resultSet.getString("Age_Joueur"));
				u.setEmail_Joueur1( resultSet.getString("Email_Joueur1"));
				u.setEmail_Joueur2( resultSet.getString("Email_Joueur2"));
				u.setTelFix_Joueur( resultSet.getString("TelFix_Joueur"));
				u.setPortable_Joueur( resultSet.getString("Portable_Joueur"));
				u.setNomPere_Joueur( resultSet.getString("NomPere_Joueur"));
				u.setNomMere_Joueur( resultSet.getString("NomMere_Joueur"));
				u.setPrenomPere_Joueur( resultSet.getString("PrenomPere_Joueur"));
				u.setPrenomMere_Joueur( resultSet.getString("PrenomMere_Joueur"));
				u.setPortablePere_Joueur( resultSet.getString("PortablePere_Joueur"));
				u.setPortableMere_Joueur( resultSet.getString("PortableMere_Joueur"));
				u.setTelFixPere_Joueur( resultSet.getString("TelFixPere_Joueur"));
				u.setTelFixMere_Joueur( resultSet.getString("TelFixMere_Joueur"));
				u.setAdresse( resultSet.getString("Adresse"));
				u.setVille( resultSet.getString("Ville"));
				u.setCode_Postal( resultSet.getInt("Code_Postal"));
				u.setId_Rang( resultSet.getInt("Id_Rang"));
				utilisateurs.add(u);
			}
			this.closeConnection();
			return utilisateurs;	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("probleme de bdd");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection();
		}
		return null;
	}
	
	
	//--------------------------------//
	//-------------POST---------------//
	//--------------------------------//
	public void insertUser(User u)
	{
		try {
			this.getConnection();
	    System.out.println("-----------------tentative de requete SQL : INSERT INTO users VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ----------------");
		PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("INSERT INTO adherent VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		preparedStatement.setInt(1, u.getId_Joueur());
	    preparedStatement.setString(2, u.getPassword());
	    preparedStatement.setString(3, u.getNom_Joueur());
	    preparedStatement.setString(4, u.getPrenom_Joueur());
	    preparedStatement.setString(5, u.getAge_Joueur());
	    preparedStatement.setString(6, u.getEmail_Joueur1());
	    preparedStatement.setString(7, u.getEmail_Joueur2());
	    preparedStatement.setString(8, u.getTelFix_Joueur());
	    preparedStatement.setString(9, u.getPortable_Joueur());
	    preparedStatement.setString(10, u.getNomPere_Joueur());
	    preparedStatement.setString(11, u.getNomMere_Joueur());
	    preparedStatement.setString(12, u.getPrenomPere_Joueur());
	    preparedStatement.setString(13, u.getPrenomMere_Joueur());
	    preparedStatement.setString(14, u.getPortablePere_Joueur());
	    preparedStatement.setString(15, u.getPortableMere_Joueur());
	    preparedStatement.setString(16, u.getTelFixPere_Joueur());
	    preparedStatement.setString(17, u.getTelFixMere_Joueur());
	    preparedStatement.setString(18, u.getAdresse());
	    preparedStatement.setString(19, u.getVille());
	    preparedStatement.setInt(20, u.getCode_Postal());
	    preparedStatement.setInt(21, u.getId_Rang());
	    preparedStatement.executeUpdate();
    	System.out.println("ok.");
	    preparedStatement.close();
	    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("probleme de bdd");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection();
		}
	}


	 //--------------------------------//
	 //-----------DELETE---------------//
	//--------------------------------//
	public User deleteUser(int Id)
	{
		try {
			this.getConnection();
	    System.out.println("-----------------tentative de requete SQL : DELETE FROM users WHERE login =? ----------------");		
		PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("DELETE FROM adherent WHERE Id =?");
		preparedStatement.setInt(1, Id);
		preparedStatement.executeUpdate();
    	System.out.println("ok.");
		preparedStatement.close();
		
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("probleme de bdd");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection();
		}
		return null;
	}
}
