package com.mkyong.rest;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.PreparedStatement;

public class FinanceDAO extends AbstractDAO{
	
	  //--------------------------------//
	 //------GET BY User ID------------//
	//--------------------------------//
	public Finance getFinanceById(int Id) throws SQLException, ClassNotFoundException{
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		System.out.println("-----------------tentative de requete SQL : select login, nom,prenom from adherent where ID=? ----------------");
		preparedStatement = (PreparedStatement) getConnection().prepareStatement("SELECT * FROM `finances` WHERE Id_Finances= ?;"); 
		preparedStatement.setInt(1, Id);
		resultSet=preparedStatement.executeQuery();
		String Finances_Notes = null;
		String Finance_Attestation = null;
		int Finance_APayer = 0;
		String Finance_RecuCotis = null;
		int Finance_SommePayee = 0;
		String Finance_PayementComplet = null;
		String Finance_NumCheque = null;
		String Finance_DepotBanque = null;
		String Finance_Renouvellement = null;
		int Id_Joueur = 0;
		while (resultSet.next()) {
			Id = resultSet.getInt("Id_Finances");
			Finances_Notes = resultSet.getString("Finances_Notes");
			Finance_Attestation= resultSet.getString("Finance_Attestation");
			Finance_APayer= resultSet.getInt("Finance_APayer");
			Finance_RecuCotis= resultSet.getString("Finance_RecuCotis");
			Finance_SommePayee= resultSet.getInt("Finance_SommePayee");
			Finance_PayementComplet= resultSet.getString("Finance_PayementComplet");
			Finance_NumCheque= resultSet.getString("Finance_NumCheque");
			Finance_DepotBanque= resultSet.getString("Finance_DepotBanque");
			Finance_Renouvellement= resultSet.getString("Finance_Renouvellement");
			Id_Joueur= resultSet.getInt("Id_Joueur");

			System.out.println(Id+Finances_Notes+Finance_Attestation+Finance_APayer+Finance_RecuCotis+
					Finance_SommePayee+Finance_PayementComplet+Finance_NumCheque+Finance_DepotBanque+
					Finance_Renouvellement+Id_Joueur);
		}	
	   return(new Finance(Id,Finances_Notes,Finance_Attestation,Finance_APayer,Finance_RecuCotis,Finance_SommePayee,
			   Finance_PayementComplet,Finance_NumCheque,Finance_DepotBanque,Finance_Renouvellement,Id_Joueur));	
	}	
	 //--------------------------------//
	 //-----------DELETE---------------//
	//--------------------------------//
	public void deleteFinance(int Id)
	{
		try {
			this.getConnection();
	    System.out.println("-----------------tentative de requete SQL : DELETE FROM users WHERE login =? ----------------");		
		PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("DELETE FROM finances WHERE Id_Joueur =?");
		preparedStatement.setInt(1, Id);
		preparedStatement.executeUpdate();
		System.out.println("ok.");
		preparedStatement.close();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("probleme de bdd");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection();
		}
	}
	//--------------------------------//
	//-------------POST---------------//
	//--------------------------------//
	public void insertFinance(Finance f)
	{
		try {
			this.getConnection();
	    System.out.println("-----------------tentative de requete SQL : INSERT INTO users VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ----------------");
		PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("INSERT INTO finances VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		preparedStatement.setInt(1, f.getId_Finances());
	    preparedStatement.setString(2, f.getFinances_Notes());
	    preparedStatement.setString(3, f.getFinance_Attestation());
	    preparedStatement.setInt(4, f.getFinance_APayer());
	    preparedStatement.setString(5, f.getFinance_RecuCotis());
	    preparedStatement.setInt(6, f.getFinance_SommePayee());
	    preparedStatement.setString(7, f.getFinance_PayementComplet());
	    preparedStatement.setString(8, f.getFinance_NumCheque());
	    preparedStatement.setString(9, f.getFinance_DepotBanque());
	    preparedStatement.setString(10, f.getFinance_Renouvellement());
	    preparedStatement.setInt(11, f.getId_Joueur());
	    preparedStatement.executeUpdate();
    	System.out.println("ok.");
	    preparedStatement.close();    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("probleme de bdd");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection();
		}
	}
}
