-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 04 Mai 2017 à 01:43
-- Version du serveur :  10.1.10-MariaDB
-- Version de PHP :  5.5.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ppeasfoot`
--

-- --------------------------------------------------------

--
-- Structure de la table `coordonnees`
--

CREATE TABLE `coordonnees` (
  `Id_Joueur` int(11) NOT NULL,
  `password` varchar(55) NOT NULL,
  `Nom_Joueur` varchar(50) DEFAULT NULL,
  `Prenom_Joueur` varchar(50) DEFAULT NULL,
  `Age_Joueur` varchar(255) DEFAULT NULL,
  `Email_Joueur1` varchar(255) DEFAULT NULL,
  `Email_Joueur2` varchar(255) DEFAULT NULL,
  `TelFix_Joueur` varchar(255) DEFAULT NULL,
  `Portable_Joueur` varchar(255) DEFAULT NULL,
  `NomPere_Joueur` varchar(50) DEFAULT NULL,
  `NomMere_Joueur` varchar(50) DEFAULT NULL,
  `PrenomPere_Joueur` varchar(50) DEFAULT NULL,
  `PrenomMere_Joueur` varchar(50) DEFAULT NULL,
  `PortablePere_Joueur` varchar(255) DEFAULT NULL,
  `PortableMere_Joueur` varchar(255) DEFAULT NULL,
  `TelFixPere_Joueur` varchar(255) DEFAULT NULL,
  `TelFixMere_Joueur` varchar(255) DEFAULT NULL,
  `Adresse` varchar(255) DEFAULT NULL,
  `Ville` varchar(50) DEFAULT NULL,
  `Code_Postal` int(11) DEFAULT NULL,
  `Id_Rang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `coordonnees`
--

INSERT INTO `coordonnees` (`Id_Joueur`, `password`, `Nom_Joueur`, `Prenom_Joueur`, `Age_Joueur`, `Email_Joueur1`, `Email_Joueur2`, `TelFix_Joueur`, `Portable_Joueur`, `NomPere_Joueur`, `NomMere_Joueur`, `PrenomPere_Joueur`, `PrenomMere_Joueur`, `PortablePere_Joueur`, `PortableMere_Joueur`, `TelFixPere_Joueur`, `TelFixMere_Joueur`, `Adresse`, `Ville`, `Code_Postal`, `Id_Rang`) VALUES
(1, 'pmolikuj', '555', '555', '555', '555', '555', '555', '555', '555', '555', '555', '555', '555', '555', '555', '555', '555', '555', 555, 1),
(2, 'pmolikuj', '666', '666', '666', '666', '666', '666', '666', '666', '666', '666', '666', '666', '666', '666', '666', '666', '666', 666, 2),
(3, 'pmolikuj', '777', '777', '777', '777', '777', '777', '777', '777', '777', '777', '777', '777', '777', '777', '777', '777', '777', 777, 3),
(4, 'pmolikuj', '888', '888', '888', '888', '888', '888', '888', '888', '888', '888', '888', '888', '888', '888', '888', '888', '888', 888, 4);

-- --------------------------------------------------------

--
-- Structure de la table `finances`
--

CREATE TABLE `finances` (
  `Id_Finances` int(11) NOT NULL,
  `Finances_Notes` varchar(255) DEFAULT NULL,
  `Finance_Attestation` varchar(255) DEFAULT NULL,
  `Finance_APayer` int(11) DEFAULT NULL,
  `Finance_RecuCotis` varchar(255) DEFAULT NULL,
  `Finance_SommePayee` int(11) DEFAULT NULL,
  `Finance_PayementComplet` varchar(255) DEFAULT NULL,
  `Finance_NumCheque` varchar(255) DEFAULT NULL,
  `Finance_DepotBanque` varchar(255) DEFAULT NULL,
  `Finance_Renouvellement` varchar(255) DEFAULT NULL,
  `Id_Joueur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Contenu de la table `finances`
--

INSERT INTO `finances` (`Id_Finances`, `Finances_Notes`, `Finance_Attestation`, `Finance_APayer`, `Finance_RecuCotis`, `Finance_SommePayee`, `Finance_PayementComplet`, `Finance_NumCheque`, `Finance_DepotBanque`, `Finance_Renouvellement`, `Id_Joueur`) VALUES
(1, '0', '0', 0, '0', 0, '0', '0', '0', '0', 1),
(2, '0', '0', 0, '0', 0, '0', '0', '0', '0', 2),
(3, '0', '0', 0, '0', 0, '0', '0', '0', '0', 3),
(4, '0', '0', 0, '0', 0, '0', '0', '0', '0', 4);

-- --------------------------------------------------------

--
-- Structure de la table `rang`
--

CREATE TABLE `rang` (
  `Id_Rang` int(11) NOT NULL,
  `Nom_Rang` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `rang`
--

INSERT INTO `rang` (`Id_Rang`, `Nom_Rang`) VALUES
(4, 'Administrateur'),
(3, 'Dirigeant'),
(2, 'secretaire'),
(1, 'Visiteur');

-- --------------------------------------------------------

--
-- Structure de la table `suivi`
--

CREATE TABLE `suivi` (
  `Id_Suivi` int(11) NOT NULL,
  `Suivi_NumLicence` varchar(255) DEFAULT NULL,
  `Suivi_Notes` varchar(255) DEFAULT NULL,
  `Suivi_DateNaissance` date DEFAULT NULL,
  `Suivi_Categorie` varchar(255) DEFAULT NULL,
  `Suivi_Dirigeant` varchar(255) DEFAULT NULL,
  `Suivi_InscritNewsletter` varchar(255) DEFAULT NULL,
  `Suivi_DispoLicence` varchar(255) DEFAULT NULL,
  `Suivi_Formation` varchar(255) DEFAULT NULL,
  `Champconfig1` varchar(255) DEFAULT NULL,
  `Champconfig2` varchar(255) DEFAULT NULL,
  `Champconfig3` varchar(255) DEFAULT NULL,
  `Champconfig4` varchar(255) DEFAULT NULL,
  `Champconfig5` varchar(255) DEFAULT NULL,
  `Suivi_Photo` varchar(255) DEFAULT NULL,
  `Id_Joueur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `suivi`
--

INSERT INTO `suivi` (`Id_Suivi`, `Suivi_NumLicence`, `Suivi_Notes`, `Suivi_DateNaissance`, `Suivi_Categorie`, `Suivi_Dirigeant`, `Suivi_InscritNewsletter`, `Suivi_DispoLicence`, `Suivi_Formation`, `Champconfig1`, `Champconfig2`, `Champconfig3`, `Champconfig4`, `Champconfig5`, `Suivi_Photo`, `Id_Joueur`) VALUES
(1, '0', '0', '0000-00-00', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 1),
(2, '0', '0', '0000-00-00', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 2),
(3, '0', '0', '0000-00-00', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 3),
(4, '0', '0', '0000-00-00', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 4);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `coordonnees`
--
ALTER TABLE `coordonnees`
  ADD PRIMARY KEY (`Id_Joueur`),
  ADD KEY `Id_Joueur` (`Id_Joueur`),
  ADD KEY `Id_Rang` (`Id_Rang`),
  ADD KEY `Id_Joueur_2` (`Id_Joueur`);

--
-- Index pour la table `finances`
--
ALTER TABLE `finances`
  ADD PRIMARY KEY (`Id_Finances`),
  ADD KEY `Id_Joueur` (`Id_Joueur`);

--
-- Index pour la table `rang`
--
ALTER TABLE `rang`
  ADD PRIMARY KEY (`Id_Rang`),
  ADD KEY `Nom_Rang` (`Nom_Rang`),
  ADD KEY `Id_Rang` (`Id_Rang`);

--
-- Index pour la table `suivi`
--
ALTER TABLE `suivi`
  ADD PRIMARY KEY (`Id_Suivi`),
  ADD UNIQUE KEY `Id_Joueur` (`Id_Joueur`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `coordonnees`
--
ALTER TABLE `coordonnees`
  MODIFY `Id_Joueur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `finances`
--
ALTER TABLE `finances`
  MODIFY `Id_Finances` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `rang`
--
ALTER TABLE `rang`
  MODIFY `Id_Rang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `suivi`
--
ALTER TABLE `suivi`
  MODIFY `Id_Suivi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `coordonnees`
--
ALTER TABLE `coordonnees`
  ADD CONSTRAINT `coordonnees_ibfk_1` FOREIGN KEY (`Id_Rang`) REFERENCES `rang` (`Id_Rang`);

--
-- Contraintes pour la table `finances`
--
ALTER TABLE `finances`
  ADD CONSTRAINT `finances_ibfk_1` FOREIGN KEY (`Id_Joueur`) REFERENCES `coordonnees` (`Id_Joueur`);

--
-- Contraintes pour la table `suivi`
--
ALTER TABLE `suivi`
  ADD CONSTRAINT `suivi_ibfk_1` FOREIGN KEY (`Id_Joueur`) REFERENCES `coordonnees` (`Id_Joueur`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
