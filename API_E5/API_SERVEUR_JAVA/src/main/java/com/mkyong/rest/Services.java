package com.mkyong.rest;
 
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import com.mkyong.rest.User;
import com.mkyong.rest.UserDAO;

 
@Path("/api")
@Consumes(MediaType.APPLICATION_JSON)
public class Services {
	
	private UserDAO uD = new UserDAO();
	private FinanceDAO fD = new FinanceDAO();
	private SuiviDao sD = new SuiviDao();
	static Gson gson = new Gson();
	
	//OK
	@GET
	@Path("users")
	public String getAllUser() throws ClassNotFoundException, SQLException
	{
    	System.out.println("-----------------tentative de getallUser----------------");
		return gson.toJson(this.uD.getAllUser());
	}
	
	//OK
	@GET
	@Path("users/{Id}")
	public String getUserById(@PathParam("Id") int Id) throws ClassNotFoundException, SQLException
	{
    	System.out.println("-----------------tentative de get User by Id----------------");
		return gson.toJson(this.uD.getUserById(Id));
	}
	
	//OK
	@GET
	@Path("suivi/{IdSuivi}")
	public String getSuiviById(@PathParam("IdSuivi") int Id) throws ClassNotFoundException, SQLException
	{
    	System.out.println("-----------------tentative de get User by Id----------------");
		return gson.toJson(this.sD.getSuiviById(Id));
	}
	
	//OK
	@GET
	@Path("finance/{IdFinance}")
	public String getFinanceById(@PathParam("IdFinance") int Id) throws ClassNotFoundException, SQLException
	{
    	System.out.println("-----------------tentative de get User by Id----------------");
		return gson.toJson(this.fD.getFinanceById(Id));
	}

	//OK
	@GET
	@Path("users/rechercher/{Nom}")
	public String getUserByName(@PathParam("Nom") String Nom) throws ClassNotFoundException, SQLException
	{
    	System.out.println("-----------------tentative de get User by Nom----------------");
		return gson.toJson(this.uD.getUserByName(Nom));
	}
	
	@POST
	@Path("add")
	public Response insertUser(User u,Suivi s, Finance f)
	{
    	System.out.println("-----------------tentative de POST----------------");
		this.uD.insertUser(u);
		this.sD.insertSuivi(s);
		this.fD.insertFinance(f);
		return Response.noContent().build();
	}
	
	@DELETE
	@Path(value ="delete/{IdDelete}")	
	public Response supprimerUtilisateur(@PathParam("IdDelete") int id)
	{
    	System.out.println("-----------------tentative de delete----------------");   	
        this.uD.deleteUser(id);
        this.sD.deleteSuivi(id);
        this.fD.deleteFinance(id);
        return Response.noContent().build();
    }
}