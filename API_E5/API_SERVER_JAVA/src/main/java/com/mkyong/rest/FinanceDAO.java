package com.mkyong.rest;

import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;

public class FinanceDAO extends AbstractDAO{
	
	  //--------------------------------//
	 //------GET BY User ID------------//
	//--------------------------------//
	public ArrayList<Finance> getFinanceById(int Id) throws SQLException, ClassNotFoundException
	{
		try{
			this.getConnection();
			System.out.println("-----------------tentative de requete SQL : SELECT * FROM adherent ----------------");
			PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("SELECT * FROM `finances` WHERE Id_Finances= ?;");
			preparedStatement.setInt(1, Id);
			resultSet = preparedStatement.executeQuery();
			ArrayList<Finance> finance = new ArrayList<Finance>();
			while (resultSet.next()) {
				System.out.println("ok.");
				Finance f = new Finance();
				f.setId_Finances(resultSet.getInt("Id_Finances"));
				f.setFinances_Notes (resultSet.getString("Finances_Notes"));
				f.setFinance_Attestation( resultSet.getString("Finance_Attestation"));
				f.setFinance_APayer( resultSet.getInt("Finance_APayer"));
				f.setFinance_RecuCotis( resultSet.getString("Finance_RecuCotis"));
				f.setFinance_SommePayee( resultSet.getInt("Finance_SommePayee"));
				f.setFinance_PayementComplet( resultSet.getString("Finance_PayementComplet"));
				f.setFinance_NumCheque( resultSet.getString("Finance_NumCheque"));
				f.setFinance_DepotBanque( resultSet.getString("Finance_DepotBanque"));
				f.setFinance_Renouvellement( resultSet.getString("Finance_Renouvellement"));
				f.setId_Joueur( resultSet.getInt("Id_Joueur"));
				finance.add(f);
			}
			this.closeConnection();
			return finance;	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("probleme de bdd");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection();
		}
		return null;
	}
	 //--------------------------------//
	 //-----------DELETE---------------//
	//--------------------------------//
	public void deleteFinance(int Id)
	{
		try {
			this.getConnection();
	    System.out.println("-----------------tentative de requete SQL : DELETE FROM users WHERE login =? ----------------");		
		PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("DELETE FROM finances WHERE Id_Joueur =?");
		preparedStatement.setInt(1, Id);
		preparedStatement.executeUpdate();
		System.out.println("ok.");
		preparedStatement.close();
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("probleme de bdd");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection();
		}
	}
	//--------------------------------//
	//-------------POST---------------//
	//--------------------------------//
	public void insertFinance(Finance f)
	{
		try {
			this.getConnection();
	    System.out.println("-----------------tentative de requete SQL : INSERT INTO users VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ----------------");
		PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("INSERT INTO finances VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		preparedStatement.setInt(1, f.getId_Finances());
	    preparedStatement.setString(2, f.getFinances_Notes());
	    preparedStatement.setString(3, f.getFinance_Attestation());
	    preparedStatement.setInt(4, f.getFinance_APayer());
	    preparedStatement.setString(5, f.getFinance_RecuCotis());
	    preparedStatement.setInt(6, f.getFinance_SommePayee());
	    preparedStatement.setString(7, f.getFinance_PayementComplet());
	    preparedStatement.setString(8, f.getFinance_NumCheque());
	    preparedStatement.setString(9, f.getFinance_DepotBanque());
	    preparedStatement.setString(10, f.getFinance_Renouvellement());
	    preparedStatement.setInt(11, f.getId_Joueur());
	    preparedStatement.executeUpdate();
    	System.out.println("ok.");
	    preparedStatement.close();    
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("probleme de bdd");
		}catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			closeConnection();
		}
	}
}
