<?php
/************************************************************
 * Script realise par Morgan ROY
 * Crée le 04/02/2017
 * Maj : ...
 * morganroy.esy.es
 *
 * Changelog:
 *
 * ...
 * .
 * version :
 * 1.0.0
 *************************************************************/
function getDB() {
    $bdd = new PDO('mysql:host=localhost;dbname=ppeasfoot', 'root', '');
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
	return $bdd;
}
?>