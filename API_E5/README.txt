//--------------------------------//
//---------Morgan ROY-------------//
//---------API WEB CRUD-----------//
//--------E4 - PPE AS FOOT--------//
//--------------------------------//
//--------------------------------//
base de donn�e : dans le fichier racine 'adherent.sql'
Connection � la base de donn�e : modele/connec_bdd.php
php : controleur/fonction.php
JAVASCRIPT : APIREST.js/mdp_tester.js

METHODES UTILISES : 

GET
POST
DELETE
PUT


  //--------------------------------//
 //-------FONCTION PHP-------------//
//--------------------------------//

GetUsers()
GetUsersByID($IdGET)
GetSuivitByID($IdGET)
GetFinanceByID($IdGET)
GetUsersByName($Nom_Joueur)

InsertUsers()

DeleteUser($Id)

ModifyUser($IdModifyUser)
ModifySuivit($IdModifySuivit)
ModifyFinance($IdModifyFinance)

  //--------------------------------//
 //-----------ROUTES---------------//
//--------------------------------//
$app->get('/users','GetUsers');
$app->get('/users/:IdGetUsersByID','GetUsersByID');
$app->get('/suivi/:IdGetSuivitByID','GetSuivitByID');
$app->get('/finance/:IdGetFinanceByID','GetFinanceByID');
$app->get('/users/rechercher/:Nom_Joueur','GetUsersByName');

$app->post('/add', 'InsertUsers');

$app->delete('/delete/:Id','DeleteUser');

$app->put('/updates/Coordonnee/:IdModifyUser','ModifyUser');
$app->put('/updates/suivit/:IdModifySuivit','ModifySuivit');
$app->put('/updates/finance/:IdModifyFinance','ModifyFinance');



