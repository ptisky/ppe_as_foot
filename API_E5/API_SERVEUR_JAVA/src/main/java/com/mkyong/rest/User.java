package com.mkyong.rest;

public class User {
	private int Id_Joueur;
	private String password;
	private String Nom_Joueur;
	private String Prenom_Joueur;
	private String Age_Joueur;
	private String Email_Joueur1;
	private String Email_Joueur2;
	private String TelFix_Joueur;
	private String Portable_Joueur;
	private String NomPere_Joueur;
	private String NomMere_Joueur;
	private String PrenomPere_Joueur;
	private String PrenomMere_Joueur;
	private String PortablePere_Joueur;
	private String PortableMere_Joueur;
	private String TelFixPere_Joueur;
	private String TelFixMere_Joueur;
	private String Adresse;
	private String Ville;
	private int Code_Postal;
	private int id_Rang;
	public User(int uid_Joueur, String upassword, String unom_Joueur, String uprenom_Joueur, String uage_Joueur,
			String uemail_Joueur1, String uemail_Joueur2, String utelFix_Joueur, String uportable_Joueur,
			String unomPere_Joueur, String unomMere_Joueur, String uprenomPere_Joueur, String uprenomMere_Joueur,
			String uportablePere_Joueur, String uportableMere_Joueur, String utelFixPere_Joueur, String utelFixMere_Joueur,
			String uadresse, String uville, int ucode_Postal, int uid_Rang) {
		this.Id_Joueur = uid_Joueur;
		this.password = upassword;
		this.Nom_Joueur = unom_Joueur;
		this.Prenom_Joueur = uprenom_Joueur;
		this.Age_Joueur = uage_Joueur;
		this.Email_Joueur1 = uemail_Joueur1;
		this.Email_Joueur2 = uemail_Joueur2;
		this.TelFix_Joueur = utelFix_Joueur;
		this.Portable_Joueur = uportable_Joueur;
		this.NomPere_Joueur = unomPere_Joueur;
		this.NomMere_Joueur = unomMere_Joueur;
		this.PrenomPere_Joueur = uprenomPere_Joueur;
		this.PrenomMere_Joueur = uprenomMere_Joueur;
		this.PortablePere_Joueur = uportablePere_Joueur;
		this.PortableMere_Joueur = uportableMere_Joueur;
		this.TelFixPere_Joueur = utelFixPere_Joueur;
		this.TelFixMere_Joueur = utelFixMere_Joueur;
		this.Adresse = uadresse;
		this.Ville = uville;
		this.Code_Postal = ucode_Postal;
		this.id_Rang = uid_Rang;
	}
	public User() {
		// TODO Auto-generated constructor stub
	}
	public int getId_Joueur() {
		return this.Id_Joueur;
	}

	public void setId_Joueur(int Monid_Joueur) {
		this.Id_Joueur = Monid_Joueur;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String Monpassword) {
		this.password = Monpassword;
	}

	public String getNom_Joueur() {
		return this.Nom_Joueur;
	}

	public void setNom_Joueur(String Monnom_Joueur) {
		this.Nom_Joueur = Monnom_Joueur;
	}

	public String getPrenom_Joueur() {
		return this.Prenom_Joueur;
	}

	public void setPrenom_Joueur(String Monprenom_Joueur) {
		this.Prenom_Joueur = Monprenom_Joueur;
	}

	public String getAge_Joueur() {
		return this.Age_Joueur;
	}

	public void setAge_Joueur(String Monage_Joueur) {
		this.Age_Joueur = Monage_Joueur;
	}

	public String getEmail_Joueur1() {
		return this.Email_Joueur1;
	}

	public void setEmail_Joueur1(String Monemail_Joueur1) {
		this.Email_Joueur1 = Monemail_Joueur1;
	}

	public String getEmail_Joueur2() {
		return this.Email_Joueur2;
	}

	public void setEmail_Joueur2(String Monemail_Joueur2) {
		this.Email_Joueur2 = Monemail_Joueur2;
	}

	public String getTelFix_Joueur() {
		return this.TelFix_Joueur;
	}

	public void setTelFix_Joueur(String MontelFix_Joueur) {
		this.TelFix_Joueur = MontelFix_Joueur;
	}

	public String getPortable_Joueur() {
		return this.Portable_Joueur;
	}

	public void setPortable_Joueur(String Monportable_Joueur) {
		this.Portable_Joueur = Monportable_Joueur;
	}

	public String getNomPere_Joueur() {
		return this.NomPere_Joueur;
	}

	public void setNomPere_Joueur(String MonnomPere_Joueur) {
		this.NomPere_Joueur = MonnomPere_Joueur;
	}

	public String getNomMere_Joueur() {
		return this.NomMere_Joueur;
	}

	public void setNomMere_Joueur(String MonnomMere_Joueur) {
		this.NomMere_Joueur = MonnomMere_Joueur;
	}

	public String getPrenomPere_Joueur() {
		return this.PrenomPere_Joueur;
	}

	public void setPrenomPere_Joueur(String MonprenomPere_Joueur) {
		this.PrenomPere_Joueur = MonprenomPere_Joueur;
	}

	public String getPrenomMere_Joueur() {
		return this.PrenomMere_Joueur;
	}

	public void setPrenomMere_Joueur(String MonprenomMere_Joueur) {
		this.PrenomMere_Joueur = MonprenomMere_Joueur;
	}

	public String getPortablePere_Joueur() {
		return this.PortablePere_Joueur;
	}

	public void setPortablePere_Joueur(String MonportablePere_Joueur) {
		this.PortablePere_Joueur = MonportablePere_Joueur;
	}

	public String getPortableMere_Joueur() {
		return this.PortableMere_Joueur;
	}

	public void setPortableMere_Joueur(String MonportableMere_Joueur) {
		this.PortableMere_Joueur = MonportableMere_Joueur;
	}

	public String getTelFixPere_Joueur() {
		return this.TelFixPere_Joueur;
	}

	public void setTelFixPere_Joueur(String MontelFixPere_Joueur) {
		this.TelFixPere_Joueur = MontelFixPere_Joueur;
	}

	public String getTelFixMere_Joueur() {
		return this.TelFixMere_Joueur;
	}

	public void setTelFixMere_Joueur(String MontelFixMere_Joueur) {
		this.TelFixMere_Joueur = MontelFixMere_Joueur;
	}

	public String getAdresse() {
		return this.Adresse;
	}

	public void setAdresse(String Monadresse) {
		this.Adresse = Monadresse;
	}

	public String getVille() {
		return this.Ville;
	}

	public void setVille(String Monville) {
		this.Ville = Monville;
	}

	public int getCode_Postal() {
		return this.Code_Postal;
	}

	public void setCode_Postal(int Moncode_Postal) {
		this.Code_Postal = Moncode_Postal;
	}

	public int getId_Rang() {
		return this.id_Rang;
	}

	public void setId_Rang(int Monid_Rang) {
		this.id_Rang = Monid_Rang;
	}
}
